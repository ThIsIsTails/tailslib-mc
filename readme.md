# TailsLib

## bStats

![TailsLib bStats](https://bstats.org/signatures/bukkit/TailsLib.svg)

## What it can do?

TailsLib library for plugins. This library allows you to do custom things that cannot be done with the vanilla Bukkit API.

## Custom items

Create your items without pain and give them crafts without much pain too.

## Custom block

Blocks that remain in your world even after a reboot and can offer basic needs in the form of eventualities with clicking on a block, breaking it, and setting it down.
They also support loading and saving! Just override 2 methods and save block datas.

### Custom effects with more functionality

Create new effects without pain.

## Can't code?

I'm currently working on `CustomWeaver` plugin based on configs so that those who can't write their own plugin can do everything through the configs.

## How to use it

Simply implement tailslib interface into your class.

```java
public class MyItem implements CustomItem {
    @Override
    public void itemData() {
        // Adding simple description to our item
        SimpleDescBuilder description = new SimpleDescBuilder()
            .addDesc("My awesome description!");
        
        /*
        You can also use List for description.
        Like: ArrayList<String> list = Arrays.asList("First", "second");

        Also you can use `null` for nothing in your description.
        */
        return new CustomItemData("item_id", "Item name", description, Material.IRON_INGOT)
            .withUUID() // Use UUID option. This item now will be created with custom UUID for every item.
            .notUniqueMaterial() // This item can be used in default crafts like iron sword.
            .addProperty(new NoPvPCustomItemProperty()); // This item can't do damage. (PVE and PVP)
    }

    /**
     *  Lets handle a right click!
     *  NOTE: rightClick and rightClickOnEntity is a diffrent thing.
     */
    @Override
    public void rightClick(PlayerInteractEvent event, UUID uuid) {
        Player player = event.getPlayer();
        player.sendMessage("Right click!");
    }
}
```

TailsLib in general uses a system of data between the whole system. So data classes can be seen everywhere.

Then register it

```java
...
@Override
public void onEnable() {
    ...
    CustomItemManager cManager = CustomItemManager.getManager();
    cManager.register(new MyItem());
    ...
}
...
```

In any case, such a scheme is basically for everything. Effects and blocks work according to the same scheme and are created in the same way.
But if you want more examples visit [this site.](https://tlibdocs.thisistails.ru/) ([direct link to gitbook](https://thisistails.gitbook.io/tailslib-simple-wiki/)).

## About versions

TailsLib does not guarntee full functionality for each version.

Each version may have a diffrent API (Like method renaming or deleting some systems).
I'll try to make compability with older versions but i'm not promise this will work perfectly.

## Wan't to use TailsLib in your project?

### Maven POM

```xml
<repositories>
    <repository>
        <id>sonatype</id>
        <url>https://oss.sonatype.org/repository/releases/</url>
    </repository>
</repositories>

<dependencies>
    <dependency>
        <groupId>ru.thisistails.tailslib</groupId>
        <artifactId>tailslib</artifactId>
        <version>VERSION</version>
        <scope>provided</scope>
    </dependency>
</dependencies>
```

### Gradle

```groovy
repositories {
    maven {
        url 'https://oss.sonatype.org/repository/releases/'
    }
}

dependencies {
    provided 'ru.thisistails.tailslib:tailslib:VERSION'
}
```

## Known issues

* Poor PAPi support (WIP)
* Don't look at the project commits or you'll have a heart attack)
