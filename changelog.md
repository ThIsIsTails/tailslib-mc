# Changelog

## 1.8-beta

I think this version is might be a stable one.

So finally TailsLib is now in beta! This version is contain many breaking changes.
I'll try to make way less breaking changes for API compability.

This version is introducing very big breaking changes, so be aware alpha versions is not compabitable with this version.

### Compability

This version is not compabitable with any version below 1.8-beta.

### Testing

**1.19**:

* No problems reported.

**1.20**:

* No problems reported.

**1.21**:

* No problems reported.

## Breaking changes

* `CustomBlock` interface now expect `PlacedBlockData` for all events. [No compability]
* `CustomItem#getItemData` renamed to `CustomItem#itemData`. [No compability]
* Renamed item property class `ICustomItemProperty` -> `CustomItemProperty`.
* `CustomItemData#asUniqueMaterial` renamed to `CustomItemData#notUniqueMaterial`. Unqiue material flag is enabled by default so we need something to disable it. [No compability]
* `CustomItem#getItemData` renamed to `CustomItem#itemData`. [No compability]
* Almost all methods in managers is now static.
* Renamed `CustomEffect#secondPassed` to `CustomEffect#process`.

## Changes

* `CustomBlock` interface now expect `PlacedBlockData` for all events.
* `CustomItemData#withUUID` is no longer a switch. Now its just enables UUID flag.
* `CustomEffectManager#getRegisteredEffects` now returns copy of set.
* `CustomEffectManager#getAppliedEffects` now returns copy of list.
* `Cooldown` now expect ticks, not seconds.
* `Cooldown#getCooldowns` now contain `Map<T, BukkitRunnable>` instead of `Map<T, BukkitTask>`.

## Fixed

* Added constructor for item data via `List<String>` (Yeah, i forgot to actually add this).
* Javadoc for `PermissionVariables` in `@return`.
* Unique material flag is now working normally.
* `CustomBlock#loadData` is now working.

## Added

* Added javadoc for `CustomBlockManager`.
* Added method `validationFailed` for `CustomBlock`. Called when manager can't validate block on position.
* Added `onItemCreation` event for `CustomItem`.
* Added `empty` and `of` static methods to `RayTraceEntitySettings` for quick creation.
* Added TailsLib Listener class (`LibListener`) for listening TailsLib events.
* Added few events for `CustomItem`.
* Added `CooldownTask`. Simple implementation of `BukkitRunnable`.
* Added `addCooldownTask` methods for `Cooldown` class.
* Added `processTick` method for `CustomEffect` as experiment. No event for LibListener provided.
* Added listeners `LibListeners` as experiment.

## 1.7-alpha

Small patch btw

### Testing

**1.20.4**:

* No problems reported.

**1.21**:

* No problems reported.

### Server breaking changes

* Permissions `tailslib.cblock.cblockplaced` renamed to `tailslib.cblock.manage`.
  (I don't think somebody cares about this change but still gonna write it here)
* Main config was updated! Make sure your config is up to date.
* Locales moved to `Locales` folder.
* Config update. Compability provided but higly recommend to regenerate config.

### Breaking changes

* `AppliedEffectData` now contain `UUID` instead of `Player`.
  Compability is provided but now player can just not be on the server.

### Fixed

* `ICustomItemProperty#itemCreated` method now working.
* `/customeffects check (PLAYER)` now work normally.

### Changes

* `Debug` class now gonna print when message is also forwarded to player.
* `Debug` class now also provide stack trace info (Class.java:line)
* `Debug` now support format. Change format in TailsLib config.
* `YAMLManager` now returns null if can't find path.
* `Phrase#setPath` is now deprecated. Use `Phrase#Phrase(String pluginName, String filePath)` instead.
* Updated javadoc for some methods in `CustomItem`
* Updated localization. (Not to much, you can ignore this change if you want.)
* Added validate function for CustomBlocks. Validate fails when block on location is air or not matching original material.
It also removes every ArmorStand in 1 block radius when deleted.
* Custom blocks is now dropping item naturally instead of giving it to player.
* First load system. Now its gonna call the load after `ServerLoadEvent` event.
  
### Added

* `citemblocklist` command for blocking items in-game.
* `CustomItemManager` now have overload methods for `blockItem` and `unblockItem`.
* Block/Unblock custom items command. (/citemblocklist)
* `Phrase#getPhrase` have overload now. You can set default phrase instead of returning null.
* `ICustomItemProperty` now can handle `entity interaction with entity` (AKA `rightClickOnEntity`) event.
* Added autosave function for CustomBlocks.
* Added option to not delete custom block on validation error.
* Added maximum duration method to `CustomEffect` class.
* Added `PermissionVariables`. Used for getting variables in permssions (More read [here](https://thisistails.gitbook.io/tailslib-simple-wiki/utilities/permission-variables))

### Removed

* `CustomBlockData` removed `dropItem` option. Use `event.setDropItems(boolean)` to specify if you need to drop item.
* `PlayerQuit` interruption reason for `CustomEffect`'s. Instead use `playerQuitWhileAppliedEffect(AppliedEffectData)`.

## 1.6-alpha

### Breaking Changes

No compatibility will be provided for this changes.
If something breaks because of this its on you.

* Custom items flags are changed to Custom Item Properties (CIP).
* * Default Item Flags are now classes. (e.g NoPvPCIP)
* Custom Items now must return `Optional<Recipe>` instead of null.
* CustomItem method `itemDamagedEnity` renamed to `itemDamageEvent`
* Added CustomItemUtils. Every static methods from CustomItemManager now in CustomItemUtils.
* Every method in CustomBlock was renamed.
* Every method in CustomEffect was renamed

### Changes

* Blocks now can cancel placedblock event to prevent placing it and return it to inventory.

### Added

* SendAll functions in ChatTools. Sends a message to all players.
* getKeyStatus in CustomItemUtils.

### Fixed

* CustomBlock manager register function.
* CustomBlock click function.
* Javadoc in YAMLManager.

### Removed

* ItemRecipeException.

## 1.5.4-beta

### Fixed

* Custom block registration.

## 1.5.3-beta

## Removed

* Some unused dependencies

## 1.5.2-beta

### Added

* New localization settings.
* `/citem` command now supports localization.

### Removed

* 'Module' system.

### Fixed

* Custom block loading and interactions. Now custom block manager requires to validate PlacedBlockData on position.
If validating is failed then PlacedBlockData will be removed.
* Custom block listener now catching exception in `CustomBlock#onCreation` and `CustomBlock#onDestroy` events.
* Custom block click events now works normally.

### Changed

* Main config. (Minor updates)
* Custom block registering now require to contain materials with `#isBlock` true.
(This change also now applied on items that representing custom block.)
