package ru.thisistails.tailslib.Utils;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.thisistails.tailslib.Tools.ChatTools;
import ru.thisistails.tailslib.Tools.YAMLManager;

/**
 * Create locale.yml or assign file with {@link #Phrase(String, String)} for your plugin to work with this class.
 * By default file with phrases is locale.yml in root directory of your plugin.
 * <p>Basicly helps you with setting up your phrases.</p>
 */
public class Phrase {

    private String plugin, filePath;
    
    /**
     * Basic constructor.
     * @param pluginName    Your plugin name defined in plugin.yml.
     */
    public Phrase(@NotNull String pluginName) {
        this.plugin = pluginName;
        this.filePath = "locale.yml";
    }

    /**
     * Basic constructor.
     * @param pluginName    Your plugin name defined in plugin.yml.
     * @param filePath      Path to locale file.
     */
    public Phrase(@NotNull String pluginName, @NotNull String filePath) {
        this.plugin = pluginName;
        this.filePath = filePath;
    }

    /**
     * Get message via {@link YAMLManager#getAndTranslateString(String, String, String)}.
     * @param path  Path to message.
     * @return      Message string with translated color codes or null if not found.
     */
    public @Nullable String getPhrase(@NotNull String path) {
        return YAMLManager.getAndTranslateString(plugin, filePath, path);
    }

    /**
     * Get message via {@link YAMLManager#getAndTranslateString(String, String, String)}.
     * @param path              Path to message.
     * @param defaultString     Message if phrase returned null.
     * @return                  Message string with translated color codes.
     */
    public @NotNull String getPhrase(@NotNull String path, @NotNull String defaultString) {
        String file = YAMLManager.getAndTranslateString(plugin, filePath, path);

        return file == null ? ChatTools.translateString(defaultString) : file;
    }

    /**
     * Sets the file path for locale file.
     * @param path  Path to locale file.
     * @deprecated  Use {@link Phrase#Phrase(String, String)} instead.
     */
    @Deprecated
    public void setPath(@NotNull String path) {
        this.filePath = path;
    }

}
