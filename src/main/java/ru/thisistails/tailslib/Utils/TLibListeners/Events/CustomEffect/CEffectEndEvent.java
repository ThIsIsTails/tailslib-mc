package ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect;

import lombok.Getter;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;

/**
 * Called when effect is ended successfully.
 */
public class CEffectEndEvent {

    @Getter
    private AppliedEffectData data;

    public CEffectEndEvent(AppliedEffectData data) {
        this.data = data;
    }
    
}
