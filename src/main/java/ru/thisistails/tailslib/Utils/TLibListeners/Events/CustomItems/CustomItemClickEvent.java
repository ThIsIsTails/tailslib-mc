package ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomItems;

import java.util.UUID;

import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.CustomItems.CustomItemUtils;

/**
 * Called when custom item recieved left or right click
 */
public class CustomItemClickEvent {

    /**
     * Represents custom item that recieved that event.
     */
    @Getter
    private CustomItem customItem;
    /**
     * Item stack that used in this event.
     */
    @Getter
    private ItemStack itemStack;
    /**
     * UUID of item stack if presented.
     */
    @Getter
    private @Nullable UUID uuidOfItem;
    @Getter
    private PlayerInteractEvent bukkitEvent;

    @Getter
    private boolean rightClick, leftClick;

    public CustomItemClickEvent(CustomItem customItem, ItemStack itemStack,
            PlayerInteractEvent bukkitEvent, boolean isLeftClick) {
        this.customItem = customItem;
        this.itemStack = itemStack;
        this.bukkitEvent = bukkitEvent;
        this.uuidOfItem = CustomItemUtils.getUUIDFromCustomItem(itemStack);
        this.leftClick = isLeftClick;
        this.rightClick = !isLeftClick;
    }
    
}
