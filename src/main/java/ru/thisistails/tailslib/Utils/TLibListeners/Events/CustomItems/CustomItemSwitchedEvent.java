package ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomItems;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import ru.thisistails.tailslib.CustomItems.CustomItem;

/**
 * Called when custom item was switched to another item.
 */
public class CustomItemSwitchedEvent implements Cancellable {

    /**
     * Who caused this event.
     */
    @Getter
    private Player player;

    /**
     * Index of old slot.
     */
    @Getter
    private int oldSlot;

    /**
     * Index of new slot.
     */
    @Getter
    private int newSlot;

    /**
     * Previous held item.
     */
    @Getter @Nullable
    private ItemStack oldItemStack;

    /**
     * Current held item.
     */
    @Getter @Nullable
    private ItemStack heldItemStack;

    /**
     * Represents custom item that recieved this event and also an owner of {@link #getHeldItemStack()}
     */
    @Getter @Nullable
    private CustomItem heldCustomItem;

    /**
     * Represents custom item that recieved this event and also an owner of {@link #getOldItemStack()}
     */
    @Getter @Nullable
    private CustomItem oldCustomItem;

    private boolean cancel;

    /**
     * Constructor for the event.
     * @param player    Who caused this event.
     * @param oldSlot   Old slot.
     * @param newSlot   New slot.
     * @param oldItemStack  Old slot item stack.
     * @param heldItemStack New slot item stack.
     * @param heldCustomItem    New slot custom item.
     * @param oldCustomItem     Old slot custom item.
     */
    public CustomItemSwitchedEvent(Player player, int oldSlot, int newSlot, @Nullable ItemStack oldItemStack,
            @Nullable ItemStack heldItemStack, @Nullable CustomItem heldCustomItem,
            @Nullable CustomItem oldCustomItem) {
        this.player = player;
        this.oldSlot = oldSlot;
        this.newSlot = newSlot;
        this.oldItemStack = oldItemStack;
        this.heldItemStack = heldItemStack;
        this.heldCustomItem = heldCustomItem;
        this.oldCustomItem = oldCustomItem;
    }

    @Override
    public boolean isCancelled() {
        return cancel;
    }
    
    @Override
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }
    
}
