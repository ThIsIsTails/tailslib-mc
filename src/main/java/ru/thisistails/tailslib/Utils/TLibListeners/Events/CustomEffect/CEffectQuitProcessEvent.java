package ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect;

import lombok.Getter;
import lombok.Setter;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;

/**
 * Called when effect is processing player quit event and deciding to save effect or not.
 */
public class CEffectQuitProcessEvent {

    /**
     * Should this effect be saved until player will join again or got canceled silently.
     */
    @Setter @Getter
    private boolean saveEffect = true;
    
    /**
     * Applied effect that processing this effect.
     * @return Data
     */
    @Getter
    private AppliedEffectData data;

    public CEffectQuitProcessEvent(AppliedEffectData data, boolean saveEffect) {
        this.data = data;
    }

}
