package ru.thisistails.tailslib.Utils.TLibListeners.Tests;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import net.kyori.adventure.text.Component;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;
import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.Utils.TLibListeners.LibListener;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectAppliedEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectEndEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectInterruptionEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectQuitProcessEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomItems.CustomItemSwitchedEvent;

public class TestListener implements LibListener {

    @Override
    public void onCustomInventoryOpened(CustomItem citem, InventoryOpenEvent event, ItemStack item) {
        
        LibListener.super.onCustomInventoryOpened(citem, event, item);
        Bukkit.broadcast(Component.text("Custom inventory opened"));
    }

    @Override
    public void onEffectApplied(CEffectAppliedEvent event) {
        
        LibListener.super.onEffectApplied(event);
        Bukkit.broadcast(Component.text("Effect Applied"));
    }

    @Override
    public void onEffectEnded(CEffectEndEvent event) {
        
        LibListener.super.onEffectEnded(event);
        Bukkit.broadcast(Component.text("Effect Ended"));
    }

    @Override
    public void onEffectInterruption(CEffectInterruptionEvent event) {
        
        LibListener.super.onEffectInterruption(event);
        Bukkit.broadcast(Component.text("Effect Interruption"));
    }

    @Override
    public void onEffectPlayerQuitProcess(CEffectQuitProcessEvent event) {
        
        LibListener.super.onEffectPlayerQuitProcess(event);
        Bukkit.broadcast(Component.text("Effect Player Quit Process"));
    }

    @Override
    public void onEffectProcess(AppliedEffectData data) {
        
        LibListener.super.onEffectProcess(data);
        Bukkit.broadcast(Component.text("Effect Process"));
    }

    @Override
    public void onInventoryOpened(CustomItem citem, InventoryOpenEvent event, ItemStack item) {
        
        LibListener.super.onInventoryOpened(citem, event, item);
        Bukkit.broadcast(Component.text("Inventory Opened"));
    }

    @Override
    public void onItemBlockedInCraftEvent(CustomItem citem, PrepareItemCraftEvent event, @Nullable UUID uuid) {
        
        LibListener.super.onItemBlockedInCraftEvent(citem, event, uuid);
        Bukkit.broadcast(Component.text("Item Blocked in Craft"));
    }

    @Override
    public void onItemCreated(CustomItem citem, CraftItemEvent event) {
        
        LibListener.super.onItemCreated(citem, event);
        Bukkit.broadcast(Component.text("Item Created"));
    }

    @Override
    public void onItemDamageEntity(CustomItem citem, @NotNull EntityDamageByEntityEvent event, @Nullable UUID uuid) {
        
        LibListener.super.onItemDamageEntity(citem, event, uuid);
        Bukkit.broadcast(Component.text("Item Damage Entity"));
    }

    @Override
    public void onItemDroppedEvent(CustomItem citem, PlayerDropItemEvent event, @Nullable UUID uuid) {
        
        LibListener.super.onItemDroppedEvent(citem, event, uuid);
        Bukkit.broadcast(Component.text("Item Dropped"));
    }

    @Override
    public void onItemLeftClick(CustomItem citem, @NotNull PlayerInteractEvent event, @Nullable UUID uuid) {
        
        LibListener.super.onItemLeftClick(citem, event, uuid);
        Bukkit.broadcast(Component.text("Item Left Click"));
    }

    @Override
    public void onItemRightClick(CustomItem citem, @NotNull PlayerInteractEvent event, @Nullable UUID uuid) {
        
        LibListener.super.onItemRightClick(citem, event, uuid);
        Bukkit.broadcast(Component.text("Item Right Click"));
    }

    @Override
    public void onItemRightClickedOnEntity(CustomItem citem, @NotNull PlayerInteractEntityEvent event, @Nullable UUID uuid) {
        
        LibListener.super.onItemRightClickedOnEntity(citem, event, uuid);
        Bukkit.broadcast(Component.text("Item Right Clicked on Entity"));
    }

    @Override
    public void onItemSwitchEvent(CustomItemSwitchedEvent event) {
        
        LibListener.super.onItemSwitchEvent(event);
        Bukkit.broadcast(Component.text("Item Switched"));
    }

    
}
