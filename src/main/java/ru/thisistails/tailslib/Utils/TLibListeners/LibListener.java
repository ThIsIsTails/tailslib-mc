package ru.thisistails.tailslib.Utils.TLibListeners;

import java.util.UUID;

import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.ApiStatus.Experimental;

import ru.thisistails.tailslib.CustomBlocks.Data.PlacedBlockData;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;
import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.CustomItems.Data.CustomItemData;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectAppliedEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectEndEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectInterruptionEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectQuitProcessEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomItems.CustomItemSwitchedEvent;

/**
 * Listener for TailsLib events.
 */
@Experimental
public interface LibListener {
    /**
     * Called when custom effect is applied.
     * <p>
     * Listener can cancel the event before its takes real effect.
     * </p>
     * @param event Event
     */
    public default void onEffectApplied(CEffectAppliedEvent event) {}

    /**
     * Called when custom effect is ended successfully.
     * @param event Event
     */
    public default void onEffectEnded(CEffectEndEvent event) {}

    /**
     * Called when custom effect is interrupted.
     * <p>
     * This action can't be cancelled.
     * </p>
     * @param event Event
     */
    public default void onEffectInterruption(CEffectInterruptionEvent event) {}

    /**
     * Called when player quit with applied effect on him.
     * This event will call for every effect on player.
     * <p>
     * This action can't be cancelled.
     * </p>
     * @param event Event
     */
    public default void onEffectPlayerQuitProcess(CEffectQuitProcessEvent event) {}

    /**
     * Called when custom effect is processing new 20 ticks.
     * <p>
     * This event will be called every second (not tick)
     * </p>
     * @param data  Data
     */
    public default void onEffectProcess(AppliedEffectData data) {}


    /**
     * Called when item is being created.
     * <p>
     * This event will not be called when item is created for {@link CustomItem#recipe(NamespacedKey, ItemStack)} event.
     * @param citem Custom item.
     * @param event Bukkit event.
     */
    public default void onItemCreated(CustomItem citem, CraftItemEvent event) {}

    /**
     * Called when item is left clicked.
     * @param citem Custom item.
     * @param event Bukkit event.
     * @param uuid  UUID of item stack if presented.
     */
    public default void onItemLeftClick(CustomItem citem, @NotNull PlayerInteractEvent event, @Nullable UUID uuid) {}

    /**
     * Called when item is right clicked.
     * @param citem Custom item.
     * @param event Bukkit event.
     * @param uuid  UUID of item stack if presented.
     */
    public default void onItemRightClick(CustomItem citem, @NotNull PlayerInteractEvent event, @Nullable UUID uuid) {}

    /**
     * Called when item damage entity.
     * @param citem Custom item.
     * @param event Bukkit event.
     * @param uuid  UUID of item stack if presented.
     */
    public default void onItemDamageEntity(CustomItem citem, @NotNull EntityDamageByEntityEvent event, @Nullable UUID uuid) {}

    /**
     * Called when item is right clicked on entity
     * @param citem Custom item.
     * @param event Bukkit event.
     * @param uuid  UUID of item stack if presented.
     */
    public default void onItemRightClickedOnEntity(CustomItem citem, @NotNull PlayerInteractEntityEvent event, @Nullable UUID uuid) {}

    /**
     * Called once for listener. Called at least 1 custom item is switched in this event.
     * @param event Event
     */
    public default void onItemSwitchEvent(CustomItemSwitchedEvent event) {}

    /**
     * Called when item is dropped by player.
     * @param citem Custom item.
     * @param event Bukkit event.
     * @param uuid  UUID of item stack if presented.
     */
    public default void onItemDroppedEvent(CustomItem citem, PlayerDropItemEvent event, @Nullable UUID uuid) {}

    /**
     * Called when item is blocking craft because have {@link CustomItemData#isUniqueMaterial()} enabled.
     * @param citem Custom item.
     * @param event Bukkit event.
     * @param uuid  UUID of item stack if presented.
     */
    public default void onItemBlockedInCraftEvent(CustomItem citem, PrepareItemCraftEvent event, @Nullable UUID uuid) {}

    /**
     * Called when inventory opened with custom item in inventory.
     * @param event Bukkit event.
     * @param item  Founded item.
     * @param citem Custom item.
     */
    public default void onInventoryOpened(CustomItem citem, InventoryOpenEvent event, ItemStack item) {}

    /**
     * Called when custom inventory opened with custom item in inventory.
     * <p>
     * This inventory is made by plugin and changing the inventory can be a bad idea.
     * </p>
     * @param event Bukkit event.
     * @param item  Founded item.
     * @param citem Custom item.
     */
    public default void onCustomInventoryOpened(CustomItem citem, InventoryOpenEvent event, ItemStack item) {}

    /**
     * Called when block is created.
     * @param data  Placed block data.
     * @param event Bukkit event.
     */
    public default void onBlockCreation(PlacedBlockData data, BlockPlaceEvent event) {}

    /**
     * Called when block is recieved destroy event.
     * @param data  Placed block data.
     * @param event Bukkit event.
     */
    public default void onBlockDestroy(PlacedBlockData data, BlockBreakEvent event) {}

    /**
     * Called when player clicks on block without item.
     * @param data  Placed block data.
     * @param player    Player that clicked on block.
     */
    public default void onRightClickOnBlock(PlacedBlockData data, Player player) {}

    /**
     * Called when player clicks on block with item.
     * @param data  Placed block data.
     * @param player    Player who clicked.
     * @param item  Item in hand.
     */
    public default void onRightClickOnBlockWithItem(PlacedBlockData data, Player player, ItemStack item) {}

}
