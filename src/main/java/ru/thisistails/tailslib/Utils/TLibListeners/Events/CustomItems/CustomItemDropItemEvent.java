package ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomItems;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.event.Cancellable;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.Setter;
import ru.thisistails.tailslib.CustomItems.CustomItem;

/**
 * Called when custom item is being dropped to the world.
 */
public class CustomItemDropItemEvent implements Cancellable {

    private boolean cancelled = false;

    /**
     * Represents custom item of item that was dropped.
     */
    @Getter
    private CustomItem customItem;
    /**
     * Item that dropped to the ground.
     */
    @Getter
    private ItemStack itemStack;
    /**
     * UUID of ItemStack if presented.
     */
    @Getter
    private @Nullable UUID uuidOfItemStack;
    /**
     * Location where its dropped.
     * @param location New location
     */
    @Getter @Setter
    private Location location;

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
    
}
