package ru.thisistails.tailslib.Utils.TLibListeners;

import java.util.HashSet;
import java.util.Set;

import org.jetbrains.annotations.ApiStatus.Experimental;

@Experimental
public class TailsLibListeners {

    private static TailsLibListeners instance;

    static {
        instance = new TailsLibListeners();
    }

    private Set<LibListener> listeners = new HashSet<>();

    private TailsLibListeners() {}

    public static Set<LibListener> getListeners() {
        return new HashSet<>(instance.listeners);
    }

    public static void register(LibListener listener) {
        instance.listeners.add(listener);
    }
    
}
