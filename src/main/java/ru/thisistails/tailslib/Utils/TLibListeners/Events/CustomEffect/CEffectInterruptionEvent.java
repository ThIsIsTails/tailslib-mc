package ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect;

import lombok.Getter;
import ru.thisistails.tailslib.CustomEffects.EffectInterruptionReason;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;

/**
 * Called when effect is interrupted. Can't be cancelled.
 * <p>
 * Called after the event
 * </p>
 */
public class CEffectInterruptionEvent {
    
    /**
     * Applied effect data that caused this event.
     * @return Data
     */
    @Getter
    private AppliedEffectData data;

    /**
     * Reason why applied effect is interrupted.
     * @return Interruption effect
     */
    @Getter
    private EffectInterruptionReason reason;

    public CEffectInterruptionEvent(AppliedEffectData data, EffectInterruptionReason reason) {
        this.data = data;
        this.reason = reason;
    }
    
}
