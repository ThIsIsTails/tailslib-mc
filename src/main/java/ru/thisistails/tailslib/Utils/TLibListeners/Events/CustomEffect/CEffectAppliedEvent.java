package ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect;

import org.bukkit.event.Cancellable;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;

public class CEffectAppliedEvent implements Cancellable {

    private boolean cancel;

    /**
     * Applied effect data of this event.
     */
    @Getter
    private AppliedEffectData data;

    /**
     * Applied effect data before this event if presented.
     */
    @Getter @Nullable
    private AppliedEffectData oldData;

    public CEffectAppliedEvent(AppliedEffectData data, @Nullable AppliedEffectData oldData) {
        this.data = data;
        this.oldData = oldData;
    }

    @Override
    public boolean isCancelled() {
        return cancel;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }
    
}
