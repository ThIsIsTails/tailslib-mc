package ru.thisistails.tailslib.Utils;

import org.bukkit.scheduler.BukkitRunnable;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;

/**
 * Simple implementation for {@link BukkitRunnable}.
 * <p>
 * This class can provide passed ticks and left ticks.
 * </p>
 */
public class CooldownTask extends BukkitRunnable {

    /**
     * Runnable that will be executed after this task.
     */
    @Getter @Nullable
    private Runnable runnable;

    /**
     * How much time initially this task was.
     * <p>
     * After this task is cancelled this field will not change.
     * </p>
     */
    @Getter
    private long time;


    /**
     * How much time left before task is done.
     * <p>
     * -1 means that task is cancelled.
     * </p>
     */
    @Getter
    private long leftTime;

    /**
     * Constructor for CooldownTask
     * @param runnable  What should be runned after this task.
     * @param time Ticks
     */
    public CooldownTask(@Nullable Runnable runnable, long time) {
        this.runnable = runnable;
        this.time = time;
        this.leftTime = time;
    }

    public CooldownTask() {}

    /**
     * Sets runnable for this task.
     * @param runnable  Runnable.
     * @return  Instance for chaining.
     */
    public CooldownTask setRunnable(@Nullable Runnable runnable) {
        this.runnable = runnable;
        return this;
    }
    

    /**
     * Sets left ticks for this task.
     * <p>
     * Be carefull with this method.
     * </p>
     * @param value How much ticks.
     * @return Instance for chaining.
     */
    public CooldownTask setTime(long value) {
        time = value;
        leftTime = value;
        return this;
    }

    @Override
    public void run() {
        leftTime--;

        if (leftTime <= 0) {
            try {
                if (runnable != null)
                    runnable.run();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                this.cancel();
            }
        }
    }

    @Override
    public synchronized void cancel() throws IllegalStateException {
        leftTime = -1;
        super.cancel();
    }
    
}
