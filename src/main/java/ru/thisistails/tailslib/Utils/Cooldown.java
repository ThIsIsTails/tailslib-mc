package ru.thisistails.tailslib.Utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.jetbrains.annotations.NotNull;

import lombok.Getter;

/**
 * A simple implementation of the cooldown for
 * Using it in different moments.
 * <p>Uses calls from {@link BukkitTask} which makes it impossible to call
 * cooldown before the server boots up. </p>
 * @see BukkitTask
 * @see BukkitRunnable
 */
public class Cooldown<T> {

    private @Getter Map<T, BukkitRunnable> cooldowns;
    private @Getter Plugin plugin;
    
    public Cooldown(String pluginName) {
        cooldowns = new ConcurrentHashMap<>();
        plugin = Bukkit.getPluginManager().getPlugin(pluginName);
        if (plugin == null) throw new NullPointerException("Plugin " + pluginName + " not found.");
    }

    /**
     * Sends T into a cooldown.
     * @param arg       Argument to send to cd
     * @param ticks   Ticks
     * @apiNote         If T already exists then this function will do nothing.
     */
    public void addNewCooldown(@NotNull T arg, long ticks) {

        if (cooldowns.containsKey(arg)) return;

        BukkitRunnable bukkitRunnable = new BukkitRunnable() {

            @Override
            public void run() {
                cooldowns.remove(arg);
            }
            
        };
        bukkitRunnable.runTaskLater(plugin, ticks);
        cooldowns.put(arg, bukkitRunnable);
    }

    /**
     * Sends T into a cooldown.
     * @param arg       Argument to send to cd
     * @param ticks   Ticks
     * @apiNote         If T already exists then this function will do nothing.
     */
    public void addNewCooldown(@NotNull T arg, long ticks, boolean rewriteIfExists) {
        if (cooldowns.containsKey(arg)) {
            if (!rewriteIfExists) return;

            cooldowns.get(arg).cancel();
        }

        BukkitRunnable bukkitRunnable = new BukkitRunnable() {

            @Override
            public void run() {
                cooldowns.remove(arg);
            }
            
        };
        bukkitRunnable.runTaskLater(plugin, ticks);
        cooldowns.put(arg, bukkitRunnable);
    }

    /**
     * Sends T into a cooldown.
     * @param arg       Argument to send to cd
     * @param ticks   Ticks
     * @param runnable  Which will be accomplished after the time is up.
     * @apiNote         If T already exists then this function will do nothing.
     */
    public void addNewCooldown(@NotNull T arg, long ticks, @NotNull Runnable runnable) {
        if (cooldowns.containsKey(arg)) return;

        BukkitRunnable bukkitRunnable = new BukkitRunnable() {

            // Перезаписываем т.к почему бы и нет)
            // Никто не будет в конце приписывать cooldown#remove
            // Все это и так знают
            @Override
            public void run() {
                cooldowns.remove(arg);
                runnable.run();
            }
            
        };
        bukkitRunnable.runTaskLater(plugin, ticks);
        cooldowns.put(arg, bukkitRunnable);
    }

    /**
     * Sends T into a cooldown.
     * @param arg       Argument to send to cd
     * @param runnable  Which will be accomplished after the time is up.
     * @apiNote         If T already exists and rewriteIfExists is true then it will cancel previous task and create a new one.
     */
    public void addNewCooldown(@NotNull T arg, long ticks, @NotNull Runnable runnable, boolean rewriteIfExists) {
        if (cooldowns.containsKey(arg)) {
            if (!rewriteIfExists) return;

            cooldowns.get(arg).cancel();
        }

        BukkitRunnable bukkitRunnable = new BukkitRunnable() {

            // Перезаписываем т.к почему бы и нет)
            // Никто не будет в конце приписывать cooldown#remove
            // Все это и так знают
            // +мы гарантируем, что кд будет удалён после истечения времени
            @Override
            public void run() {
                cooldowns.remove(arg);
                runnable.run();
            }
            
        };
        bukkitRunnable.runTaskLater(plugin, ticks);
        cooldowns.put(arg, bukkitRunnable);
    }

    /**
     * Sends T into cooldown via {@link CooldownTask} class.
     * <p>
     * {@link CooldownTask} will run every tick but this means you can get ticks left via
     * {@link CooldownTask#getLeftTime()}.
     * </p>
     * @param arg
     * @param ticks
     */
    public void addCooldownTask(@NotNull T arg, long ticks) {
        CooldownTask task = new CooldownTask().setTime(ticks);
        task.runTaskTimer(plugin, 0, 1);

        cooldowns.put(arg, task);
    }

    /**
     * Removes T from cooldown.
     * <p>
     * Will silently fail when got null or cooldown not exists.
     * </p>
     * @param arg   T like argument.
     */
    public void removeCooldown(@NotNull T arg) {
        if (arg == null || !containsCooldown(arg)) return;
        cooldowns.get(arg).cancel();
        cooldowns.remove(arg);
    }

    public boolean containsCooldown(T arg) {
        return cooldowns.containsKey(arg);
    }

}
