package ru.thisistails.tailslib.CustomItems.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.inventory.RecipeChoice.ExactChoice;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.CustomItems.CustomItemProperty;
import ru.thisistails.tailslib.Utils.Description.IDescBuilder;

/**
 * Data class for CustomItem class.
 */
@Data
public class CustomItemData {
    
    private @NotNull @Setter(AccessLevel.NONE) String id;
    private @NotNull @Setter(AccessLevel.NONE) String name;
    private @NotNull @Setter(AccessLevel.NONE) List<String> lore;
    private @NotNull @Setter(AccessLevel.NONE) Material material;
    private @NotNull @Setter(AccessLevel.NONE) Set<CustomItemProperty> properties;

    /**
     * Constructor for Item Data.
     * @param id        ID used in manager.
     * @param name      Display name of this item. (Supports HEX codes and will translate color codes.)
     * @param lore      Lore (Description) of this item used in the game. Can be null.
     * @param material  Material (AKA Item) of this item used in the game.
     */
    public CustomItemData(@NotNull String id, @NotNull String name, @Nullable IDescBuilder lore, @NotNull Material material) {
        this.id = id;
        this.name = name;
        this.lore = lore == null ? new ArrayList<>() : lore.build();
        this.material = material;
        properties = new HashSet<>();
    }

    /**
     * Constructor for Item Data.
     * @param id    ID used in manager.
     * @param name  Display name of this item. (Supports HEX codes and will translate color codes.)
     * @param lore  Lore (Description) of this item used in the game. Can be null.
     * @param material Material (AKA Item) of this item used in the game.
     */
    public CustomItemData(@NotNull String id, @NotNull String name, @Nullable List<String> lore, @NotNull Material material) {
        this.id = id;
        this.name = name;
        this.lore = lore == null ? new ArrayList<>() : lore;
        this.material = material;
        properties = new HashSet<>();
    }

    /**
     * Means that item no longer can used in oridnary crafts like diamond in diamond sword craft.
     * In our case we don't want to use it in craft like this.
     * <p>Example: Your item material is iron and this setting is true.
     * Players can't craft iron sword with your item but can craft something in custom crafts where {@link ExactChoice} is used.</p>
     */
    @Setter(AccessLevel.NONE)
    @Nullable
    private boolean uniqueMaterial = true;

    /**
     * Unmark unique material flag. By default this option is enabled.
     * <p>
     * Means that item no longer can used in oridnary crafts like diamond in diamond sword craft.
     * In our case we don't want to use it in craft like this.
     * </p>
     * <p>Example: Your item material is iron and this setting is true.
     * Players can't craft iron sword with your item but can craft something in custom crafts where {@link ExactChoice} is used.</p>
     * @return this instance for chain.
     */
    public CustomItemData notUniqueMaterial() {
        uniqueMaterial = false;

        return this;
    }

    /**
     * Means that tailslib should use UUID for items.
     * That also means UUID argument in functions like {@link CustomItem#rightClick(PlayerInteractEvent, UUID)} would never be null.
     */
    @Setter(AccessLevel.NONE)
    private boolean uniqueId = false;

    /**
     * Mark/Unmark uuid option. By default this option is disabled.
     * 
     * <p>Means that tailslib should use UUID for items.
     * That also means UUID argument in functions like {@link CustomItem#rightClick(PlayerInteractEvent, UUID)} would never be null.</p>
     */
    public CustomItemData withUUID() {
        uniqueId = true;

        return this;
    }

    /**
     * Adds a property to your item.
     * @param property  Property
     * @return          This class for chain.
     * @throws IllegalStateException If property require UUID but requirements not met.
     * @throws IllegalArgumentException If some properties fails validation.
     */
    public CustomItemData addProperty(@NotNull CustomItemProperty property) {
        if (property.requireUUIDFromItems() && !isUniqueId())
            throw new IllegalStateException("Property " + property.name() + " (By " + property.author() + "; Ver. " + property.version() + ") require a UUID option.");

        properties.add(property);

        return this;
    }

    /**
     * Adds a properties to your item.
     * @param properties  Properties
     * @return          This class for chain.
     * @throws IllegalStateException If property require UUID but requirements not met.
     * @throws IllegalArgumentException If some properties fails validation.
     */
    public CustomItemData addProperties(@NotNull CustomItemProperty... properties) {
        for (CustomItemProperty prop : properties) {
            addProperty(prop);
        }

        return this;
    }

    /**
     * Checks for property
     * @param id Property ID. (By default its no spaces with lower case. E.g: "No PvP Property" -> "nopvpproperty_author")
     * @return
     */
    public boolean containProperty(@NotNull String id) {
        return properties.stream().anyMatch((prop) -> prop.id().equals(id));
    }

    /**
     * Returns required property if presented.
     * @param id ID of required property
     * @return   Property with this ID
     */
    public @Nullable CustomItemProperty getProperty(String id) {
        for (CustomItemProperty prop : properties)
            if (prop.id().equals(id)) return prop;
        
        return null;
    }

}
