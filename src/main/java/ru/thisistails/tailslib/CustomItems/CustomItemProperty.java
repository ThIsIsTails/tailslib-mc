package ru.thisistails.tailslib.CustomItems;

import java.util.UUID;

import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.thisistails.tailslib.CustomItems.Data.CustomItemData;

/**
 * Base class for item properties.
 */
public interface CustomItemProperty {

    /**
     * Property name.
     * @return Name of this property.
     */
    public @NotNull String name();
    /**
     * Author.
     * @return  Author of this property.
     */
    public @NotNull String author();
    /**
     * Version of this property.
     * @return Version.
     * @implNote I recommend to use 3 digits (0.0.0)
     */
    public @NotNull String version();

    /**
     * ID used in checks.
     * @return Lower case name with no spaces + _author. (e.g nopvp_thisistails)
     * @apiNote I do NOT recommend you to change this method. Just don't touch it, okay?
     */
    public default @NotNull String id() {
        return name().replaceAll(" ", "").toLowerCase() + "_" + author().replaceAll(" ", "").toLowerCase();
    } 

    /**
     * Validate for custom properties or something else.
     * @param data  Item data.
     * @return      null if everything is okay otherwise returns a error message to be displayed.
     * @implNote You CAN'T get CustomItem class with this because this will be called on register.
     */
    public default @Nullable String validateItem(CustomItemData data) { return null; }

    /**
     * This method will decide if CustomItem need to enable {@link CustomItemData#setShouldBeUnique(boolean)}.
     * <p> 
     * If this method returns true and CustomItem does not meet this criteria then item will not be registered
     * with IllegalStateException.
     * </p>
     * @return  Required
     */
    public boolean requireUUIDFromItems();

    // TODO: Handle other events.

    /**
     * Item with that property has been created.
     * <p>
     * Property 
     * @param createdItem   Created item. (You can get CustomItem via {@link CustomItemManager#tryGetCItemFromItemStack(ItemStack)})
     * @param uuid          UUID if presented.
     */
    public default void itemCreated(@NotNull ItemStack createdItem, @Nullable UUID uuid) {}

    /**
     * Method to handle when entity damage entity by item with this property.
     * @param usedItem  Custom item that used.
     * @param uuid      UUID if presented
     * @param event     Event
     */
    public default void itemDamageEntity(@NotNull ItemStack usedItem, @Nullable UUID uuid, EntityDamageByEntityEvent event) {}
    
}
