package ru.thisistails.tailslib.CustomItems.Tests;

import java.util.Optional;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.CustomItems.CustomItemUtils;
import ru.thisistails.tailslib.CustomItems.Data.CustomItemData;
import ru.thisistails.tailslib.Utils.Description.SimpleDescBuilder;

public class SimpleCustomBlockItem implements CustomItem {

    @Override
    public @NotNull CustomItemData itemData() {
        CustomItemData customItemData = new CustomItemData("simplecustomitemforblock", "Simple block item", 
            new SimpleDescBuilder().setOther("Thug shaker central"), Material.GLASS);
        return customItemData;
    }

    @Override
    public @NotNull Optional<Recipe> recipe(NamespacedKey key, ItemStack item) {
        ShapelessRecipe r = new ShapelessRecipe(key, item);
        r.addIngredient(CustomItemUtils.getExactChoiceFromID("testitem"));
        return Optional.of(r);
    }

    @Override
    public void leftClick(@NotNull PlayerInteractEvent event, @Nullable UUID uuid) {
        event.getPlayer().sendMessage("Yo, just place ME!!!");
    }
    
}
