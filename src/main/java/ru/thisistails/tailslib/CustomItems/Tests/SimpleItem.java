package ru.thisistails.tailslib.CustomItems.Tests;

import java.util.Optional;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.jetbrains.annotations.NotNull;

import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.CustomItems.CustomItemUtils;
import ru.thisistails.tailslib.CustomItems.Data.CustomItemData;
import ru.thisistails.tailslib.CustomItems.ItemProperties.NoPvPCustomItemProperty;
import ru.thisistails.tailslib.Tools.Debug;
import ru.thisistails.tailslib.Tools.PermissionVariables;
import ru.thisistails.tailslib.Utils.Description.SimpleDescBuilder;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomItems.CustomItemSwitchedEvent;

public class SimpleItem implements CustomItem {

    @Override
    public void leftClick(PlayerInteractEvent event, UUID uuid) {
        event.getPlayer().sendMessage("Wow, left click.");
        Debug.info(event.getPlayer(), "Fetched UUID: " + String.valueOf(uuid));

        Inventory inventory = Bukkit.createInventory(null, 9);
        inventory.addItem(CustomItemUtils.createItem(this));
        event.getPlayer().openInventory(inventory);
    }

    @Override
    public void rightClick(PlayerInteractEvent event, UUID uuid) {
        Debug.info(event.getPlayer(), "Left click debug message");
        Debug.warn(event.getPlayer(), "Left click debug message");
        Debug.error(event.getPlayer(), "Left click debug message");

        PermissionVariables.getIntPermissionVariable(event.getPlayer(), "tailslib.test.int.%var%.skibidi")
            .ifPresentOrElse((i) -> Debug.info(event.getPlayer(), String.valueOf(i)), () -> Debug.info(event.getPlayer(), "[tailslib.test.int.%var%.skibidi] No permission found."));
        PermissionVariables.getStringPermissionVariable(event.getPlayer(), "tailslib.test.string.%var%.skibidi")
            .ifPresentOrElse((i) -> Debug.info(event.getPlayer(), String.valueOf(i)), () -> Debug.info(event.getPlayer(), "[tailslib.test.string.%var%.skibidi] No permission found."));
        PermissionVariables.getBooleanPermissionVariable(event.getPlayer(), "tailslib.test.bool.%var%.skibidi")
            .ifPresentOrElse((i) -> Debug.info(event.getPlayer(), String.valueOf(i)), () -> Debug.info(event.getPlayer(), "[tailslib.test.bool.%var%.skibidi] No permission found."));
    }

    @Override
    public @NotNull CustomItemData itemData() {

        CustomItemData data = new CustomItemData("testitem", "Test item", new SimpleDescBuilder()
            .addDesc("Test description"), 
            Material.IRON_AXE)
            .addProperty(new NoPvPCustomItemProperty());

        return data;
    }

    @Override
    public @NotNull Optional<Recipe> recipe(NamespacedKey key, ItemStack item) {
        ShapelessRecipe r = new ShapelessRecipe(key, item);
        r.addIngredient(Material.STICK);
        return Optional.of(r);
    }

    @Override
    public void customInventoryOpened(InventoryOpenEvent event, ItemStack item) {
        event.getPlayer().sendMessage("Custom inventory opened!");
    }

    @Override
    public void inventoryOpened(InventoryOpenEvent event, ItemStack item) {
        event.getPlayer().sendMessage("Normal inventory opened!");
    }

    @Override
    public void itemSelectEvent(CustomItemSwitchedEvent event) {
        event.getPlayer().sendMessage("Im new selected item! " + String.valueOf(event.getNewSlot()));
    }

    @Override
    public void itemSwitchEvent(CustomItemSwitchedEvent event) {
        event.getPlayer().sendMessage("I have been switched to another item ): " + String.valueOf(event.getOldSlot()));
    }
    
}
