package ru.thisistails.tailslib.CustomItems;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import lombok.extern.slf4j.Slf4j;
import net.kyori.adventure.text.Component;
import ru.thisistails.tailslib.Localization;
import ru.thisistails.tailslib.CustomItems.CustomItemUtils.CustomItemKeyStatus;
import ru.thisistails.tailslib.Tools.Config;
import ru.thisistails.tailslib.Tools.Debug;
import ru.thisistails.tailslib.Utils.TLibListeners.LibListener;
import ru.thisistails.tailslib.Utils.TLibListeners.TailsLibListeners;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomItems.CustomItemSwitchedEvent;

/**
 * Listener for custom items.
 */
@Slf4j
public class CustomItemListener implements Listener {

    public CustomItemListener() {}

    @EventHandler
    public void inventoryOpened(InventoryOpenEvent event) {
        InventoryHolder holder = event.getInventory().getHolder(false);
        Bukkit.broadcast(Component.text(event.getInventory().getType().toString()));

        Map<CustomItem, ItemStack> foundedMap = new HashMap<>();

        for (ItemStack contents : event.getInventory().getContents()) {
            CustomItem citem = CustomItemUtils.tryGetCItemFromItemStack(contents);
            if (citem == null) continue;

            foundedMap.put(citem, contents);
        }

        if (holder == null || holder.equals(event.getPlayer()) || !(event.getInventory().getType().equals(InventoryType.ENDER_CHEST))) {
                
            for (Entry<CustomItem,ItemStack> entrySet : foundedMap.entrySet()) {
                for (LibListener listeners : TailsLibListeners.getListeners()) {
                    try {
                        listeners.onCustomInventoryOpened(entrySet.getKey(), event, entrySet.getValue());
                    } catch (Exception error) {
                        log.error("Failed to execute listener on custom inventory opened event: " + listeners.getClass().getName(), error);
                    }
                }

                try {
                    entrySet.getKey().customInventoryOpened(event, entrySet.getValue());
                } catch (Exception error) {
                    somethingWentWrong(event.getPlayer());
                    log.error("Error while executing custom inventory open event for " + entrySet.getKey().itemData().getId(), error);
                }
            }

            return;
        }

        for (Entry<CustomItem,ItemStack> entrySet : foundedMap.entrySet()) {
            for (LibListener listeners : TailsLibListeners.getListeners()) {
                try {
                    listeners.onInventoryOpened(entrySet.getKey(), event, entrySet.getValue());
                } catch (Exception error) {
                    log.error("Failed to execute listener on inventory opened event: " + listeners.getClass().getName(), error);
                }
            }

            try {
                entrySet.getKey().inventoryOpened(event, entrySet.getValue());
            } catch (Exception error) {
                somethingWentWrong(event.getPlayer());
                log.error("Error while executing inventory open event for " + entrySet.getKey().itemData().getId(), error);
            }
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void clicks(PlayerInteractEvent event) {
        ItemStack item = event.getItem();
        Action action = event.getAction();
        Player player = event.getPlayer();

        if (item == null)
            return;

        CustomItemKeyStatus keyStatus = CustomItemUtils.getKeyStatus(item);

        if (keyStatus == CustomItemKeyStatus.FoundButNotRegistered) {
            // Пишем, что предмет использует TailsLib, но он не зарегистрирован в нём.
            Debug.error(player, Localization.prefix + " " + Localization.itemNotRegistered
                .replace("%customitem_id%", CustomItemUtils.tryGetIDFromItemStack(item))
            );
            return;
        }
            if (keyStatus == CustomItemKeyStatus.FoundButNotRegistered) {
                // Пишем, что предмет использует TailsLib, но он не зарегистрирован в нём.
                Debug.error(player, Localization.prefix + " " + Localization.itemNotRegistered
                    .replace("%customitem_id%", CustomItemUtils.tryGetIDFromItemStack(item))
                );
                return;
            }

            if (keyStatus == CustomItemKeyStatus.Found) {
                CustomItem citem = CustomItemUtils.tryGetCItemFromItemStack(item);


                if (action.equals(Action.LEFT_CLICK_AIR) || action.equals(Action.LEFT_CLICK_BLOCK)) {
                    if (citem.itemData().containProperty("disableactions_thisistails")) {
                        event.setCancelled(true);
                    }

                    for (LibListener listeners : TailsLibListeners.getListeners()) {
                        try {
                            listeners.onItemLeftClick(citem, event, CustomItemUtils.getUUIDFromCustomItem(item));
                        } catch (Exception error) {
                            log.error("Failed to execute listener on item damaged event: " + listeners.getClass().getName(), error);
                        }
                    }

                    try {
                        citem.leftClick(event, CustomItemUtils.getUUIDFromCustomItem(item));
                    } catch (Exception error) {
                        player.sendMessage(Localization.prefix + " " + Localization.somethingWentWrong);
                        error.printStackTrace();
                    }
                    return;
                }

                if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
                    if (citem.itemData().containProperty("disableactions_thisistails")) {
                        event.setCancelled(true);
                    }

                    for (LibListener listeners : TailsLibListeners.getListeners()) {
                        try {
                            listeners.onItemRightClick(citem, event, CustomItemUtils.getUUIDFromCustomItem(item));
                        } catch (Exception error) {
                            log.error("Failed to execute listener on item damaged event: " + listeners.getClass().getName(), error);
                        }
                    }

                    try {
                        citem.rightClick(event, CustomItemUtils.getUUIDFromCustomItem(item));
                    } catch (Exception error) {
                        player.sendMessage(Localization.prefix + " " + Localization.somethingWentWrong);
                        error.printStackTrace();
                    }
                    return;
                }
            }

    }

    @EventHandler
    public void onCraft(PrepareItemCraftEvent event) {
        for (ItemStack ingredient : event.getInventory().getMatrix()) {
            if (ingredient == null) continue;

            CustomItem citem = CustomItemUtils.tryGetCItemFromItemStack(ingredient);

            if (citem == null) continue;

            UUID uuid = CustomItemUtils.getUUIDFromCustomItem(ingredient);

            try {

                citem.itemBlockedInCraftEvent(event, uuid);

            } catch (Exception error) {

                if (!event.getViewers().isEmpty()) {
                    event.getViewers().forEach(this::somethingWentWrong);
                }

                log.error("Got error while executing itemBlockedInCraftEvent; Custom item id: " + citem.itemData().getId(), error);

            }
            if (citem.itemData().isUniqueMaterial()) {
                event.getInventory().setResult(null);
                break;
            }
        }
    }

    @EventHandler
    public void onItemSwitch(PlayerItemHeldEvent event) {
        int newSlot = event.getNewSlot();
        int oldSlot = event.getPreviousSlot();

        Player player = event.getPlayer();

        ItemStack currentSlotItem, oldSlotItem;
        currentSlotItem = player.getInventory().getItem(newSlot);
        oldSlotItem = player.getInventory().getItem(oldSlot);

        CustomItem newCitem, oldCitem;

        newCitem = CustomItemUtils.tryGetCItemFromItemStack(currentSlotItem);
        oldCitem = CustomItemUtils.tryGetCItemFromItemStack(oldSlotItem);

        if (newCitem == null && oldCitem == null) return;

        CustomItemSwitchedEvent cevent = new CustomItemSwitchedEvent(player, oldSlot, newSlot, oldSlotItem, currentSlotItem, newCitem, oldCitem);

        for (LibListener listeners : TailsLibListeners.getListeners()) {
            try {
                listeners.onItemSwitchEvent(cevent);
            } catch (Exception error) {
                log.error("Failed to execute listener on item damaged event: " + listeners.getClass().getName(), error);
            }
        }

        try {
            if (oldSlotItem != null) {
                Bukkit.broadcast(Component.text(oldCitem.itemData().getId()));
                oldCitem.itemSwitchEvent(cevent);
            }
        } catch (Exception error) {
            somethingWentWrong(player);
            log.error("Failed to execute action for old slot custom item with ID: " + oldCitem.itemData().getId(), error);
        }

        try {
            if (newCitem != null) {
                newCitem.itemSelectEvent(cevent);
            }
        } catch (Exception error) {
            somethingWentWrong(player);
            log.error("Failed to execute action for new slot custom item with ID: " + oldCitem.itemData().getId(), error);
        }

        event.setCancelled(cevent.isCancelled());
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent event) {
        CustomItem citem = CustomItemUtils.tryGetCItemFromItemStack(event.getItemDrop().getItemStack());

        if (citem == null) return;

        UUID uuid = CustomItemUtils.getUUIDFromCustomItem(event.getItemDrop().getItemStack());

        for (LibListener listeners : TailsLibListeners.getListeners()) {
            try {
                listeners.onItemDroppedEvent(citem, event, uuid);
            } catch (Exception error) {
                log.error("Failed to execute listener on item dropped event: " + listeners.getClass().getName(), error);
            }
        }

        try {
            citem.itemDroppedEvent(event, uuid);
        } catch (Exception error) {
            this.somethingWentWrong(event.getPlayer());
            log.error("Failed to execute action for custom item with ID: " + citem.itemData().getId(), error);
        }
    }

    @EventHandler
    public void onSuccessfullCraft(CraftItemEvent event) {
        CustomItem citem = CustomItemUtils.tryGetCItemFromItemStack(event.getRecipe().getResult());
        if (citem == null) return;

        for (LibListener listeners : TailsLibListeners.getListeners()) {
            try {
                listeners.onItemCreated(citem, event);
            } catch (Exception error) {
                log.error("Failed to execute listener on item created event: " + listeners.getClass().getName(), error);
            }
        }

        try {
            citem.itemCreation(event);
        } catch (Exception error) {
            somethingWentWrong(event.getWhoClicked());
            log.error("Failed to execute action for old custom item with ID: " + citem.itemData().getId(), error);
        }

        if (!event.isCancelled()) {
            if (CustomItemUtils.getUUIDFromCustomItem(event.getCurrentItem()) == null && citem.itemData().isUniqueId()) {
                ItemStack stack = CustomItemManager.getManager().putUUID(event.getCurrentItem());
                event.setCurrentItem(stack);
            }
        }
    }

    @EventHandler
    public void checkItem(InventoryClickEvent event) {
        if (event.getCurrentItem() == null) return;
        CustomItem item = CustomItemUtils.tryGetCItemFromItemStack(event.getCurrentItem());
        if (item == null) return;

        if (CustomItemUtils.getUUIDFromCustomItem(event.getCurrentItem()) == null && item.itemData().isUniqueId()) {
            ItemStack stack = CustomItemManager.getManager().putUUID(event.getCurrentItem());
            event.setCurrentItem(stack);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void damage(EntityDamageByEntityEvent event) {

        if (!(event.getDamager() instanceof Player)) return;

        Player damager = (Player) event.getDamager();
        ItemStack heldItem = damager.getInventory().getItemInMainHand();
        ItemStack offHandHeldItem = damager.getInventory().getItemInOffHand();

        CustomItemKeyStatus keyStatus;

        if (heldItem == null)
            keyStatus = CustomItemKeyStatus.NotFound;
        else
            keyStatus = CustomItemUtils.getKeyStatus(heldItem);

        // Проверяем основную руку на кастомный предмет и выполняем с ней действия
        if (keyStatus == CustomItemKeyStatus.FoundButNotRegistered) {
            // Пишем, что предмет использует TailsLib, но он не зарегистрирован в нём.
            Debug.error(damager.getPlayer(), Localization.prefix + " " + Localization.itemNotRegistered
                .replace("%customitem_id%", CustomItemUtils.tryGetIDFromItemStack(heldItem))
            );
            return;
        }

        if (keyStatus == CustomItemKeyStatus.Found) {
            CustomItem citem = CustomItemUtils.tryGetCItemFromItemStack(heldItem);
            if (citem.itemData().containProperty("disableactions_thisistails")) {
                event.setCancelled(true);
            }

            // I know that nopvp property already can cancel this event but im to lazy.
            if (citem.itemData().containProperty("nopvp_thisistails")) {
                event.setCancelled(true);
            }

            for (LibListener listeners : TailsLibListeners.getListeners()) {
                try {
                    listeners.onItemDamageEntity(citem, event, CustomItemUtils.getUUIDFromCustomItem(heldItem));
                } catch (Exception error) {
                    log.error("Failed to execute listener on item damaged event: " + listeners.getClass().getName(), error);
                }
            }

            try {
                citem.itemDamageEntity(event, CustomItemUtils.getUUIDFromCustomItem(heldItem));
            } catch (Exception error) {
                somethingWentWrong(damager);
                error.printStackTrace();
            }
        }

        if (offHandHeldItem == null)
            keyStatus = CustomItemKeyStatus.NotFound;
        else
            keyStatus = CustomItemUtils.getKeyStatus(offHandHeldItem);

        // Тоже самое для вторичной руки
        if (keyStatus == CustomItemKeyStatus.FoundButNotRegistered) {
            // Пишем, что предмет использует TailsLib, но он не зарегистрирован в нём.
            Debug.error(damager.getPlayer(), Localization.prefix + " " + Localization.itemNotRegistered
                .replace("%customitem_id%", CustomItemUtils.tryGetIDFromItemStack(offHandHeldItem))
            );
            return;
        }

        if (keyStatus == CustomItemKeyStatus.Found) {
            CustomItem citem = CustomItemUtils.tryGetCItemFromItemStack(offHandHeldItem);

            for (CustomItemProperty prop : citem.itemData().getProperties())
                prop.itemDamageEntity(offHandHeldItem, CustomItemUtils.getUUIDFromCustomItem(offHandHeldItem), event);
            
            for (LibListener listeners : TailsLibListeners.getListeners()) {
                try {
                    listeners.onItemDamageEntity(citem, event, CustomItemUtils.getUUIDFromCustomItem(offHandHeldItem));
                } catch (Exception error) {
                    log.error("Failed to execute listener on item damaged event: " + listeners.getClass().getName(), error);
                }
            }
                    
            try {
                citem.itemDamageEntity(event, CustomItemUtils.getUUIDFromCustomItem(offHandHeldItem));
            } catch (Exception error) {
                somethingWentWrong(damager);
                error.printStackTrace();
            }
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInteractionEventWithEntity(PlayerInteractEntityEvent event) {
        ItemStack item = event.getPlayer().getActiveItem();

        if (item == null)
            return;

        @NotNull
        CustomItemKeyStatus keyStatus = CustomItemUtils.getKeyStatus(item);

        if (keyStatus == CustomItemKeyStatus.FoundButNotRegistered) {
            // Пишем, что предмет использует TailsLib, но он не зарегистрирован в нём.
            Debug.error(event.getPlayer(), Localization.prefix + " " + Localization.itemNotRegistered
                .replace("%customitem_id%", CustomItemUtils.tryGetIDFromItemStack(item))
            );
            return;
        }

        if (keyStatus == CustomItemKeyStatus.Found) {
            CustomItem citem = CustomItemUtils.tryGetCItemFromItemStack(item);

            for (LibListener listeners : TailsLibListeners.getListeners()) {
                try {
                    listeners.onItemRightClickedOnEntity(citem, event, CustomItemUtils.getUUIDFromCustomItem(item));
                } catch (Exception error) {
                    log.error("Failed to execute listener on item damaged event: " + listeners.getClass().getName(), error);
                }
            }

            try {
                citem.rightClickOnEntity(event, CustomItemUtils.getUUIDFromCustomItem(item));
            } catch (Exception error) {
                this.somethingWentWrong(event.getPlayer());
                error.printStackTrace();
            }
        }
    }

    private void somethingWentWrong(HumanEntity entity) {
        if (Config.getConfig().getBoolean("debug.supressErrors")) {
            entity.sendMessage(Localization.prefix + " " + Localization.somethingWentWrong  );
        }
    }

}
