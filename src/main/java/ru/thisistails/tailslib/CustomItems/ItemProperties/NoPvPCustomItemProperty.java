package ru.thisistails.tailslib.CustomItems.ItemProperties;

import ru.thisistails.tailslib.CustomItems.CustomItemProperty;

/**
 * Built-in property.
 * <p>
 * Cancels PVP and PVE actions for custom items.
 * </p>
 */
public class NoPvPCustomItemProperty implements CustomItemProperty {

    @Override
    public String name() {
        return "No PVP";
    }

    @Override
    public String author() {
        return "ThIsIsTails";
    }

    @Override
    public String version() {
        return "1.0.0";
    }

    @Override
    public boolean requireUUIDFromItems() {
        return false;
    }
    
}
