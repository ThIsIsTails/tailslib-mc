package ru.thisistails.tailslib.CustomItems.ItemProperties;

import org.jetbrains.annotations.NotNull;

import ru.thisistails.tailslib.CustomItems.CustomItemProperty;

/**
 * Built-in item property.
 * <p>
 * This property disables every action for item. (Actually not, but you get the idea.)
 * </p>
 */
public class NoActionsCIP implements CustomItemProperty {

    @Override
    public @NotNull String name() {
        return "Disable Actions";
    }

    @Override
    public @NotNull String author() {
        return "ThIsIsTails";
    }

    @Override
    public @NotNull String version() {
        return "1.0.0";
    }

    @Override
    public boolean requireUUIDFromItems() {
        return false;
    }
    
}
