package ru.thisistails.tailslib.CustomItems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import net.kyori.adventure.text.Component;
import net.md_5.bungee.api.ChatColor;
import ru.thisistails.tailslib.Exceptions.IDPatternException;
import ru.thisistails.tailslib.Tools.Config;
import ru.thisistails.tailslib.Tools.YAMLManager;

public class CustomItemManager {
    
    private @Getter Map<String, CustomItem> items;
    private static CustomItemManager instance;
    private @Getter NamespacedKey customItemKey = new NamespacedKey(Bukkit.getPluginManager().getPlugin("TailsLib"), "customitem");
    private @Getter NamespacedKey uuidKey = new NamespacedKey(Bukkit.getPluginManager().getPlugin("TailsLib"), "customitemuuid");
    private static final Logger log = LoggerFactory.getLogger("CustomItemManager");

    static {
        instance = new CustomItemManager();
    }

    private Pattern idPattern = Pattern.compile("^[a-z_]+$");
    
    private @Getter List<CustomItem> blacklistedItems = new ArrayList<>();

    private CustomItemManager() {
        items = new HashMap<>();
    }

    public static CustomItemManager getManager() {     
        return instance;
    }

    protected ItemStack putUUID(ItemStack stack) {
        ItemStack item = stack.clone();
        if (item.getItemMeta() == null) return null;

        ItemMeta meta = item.getItemMeta();
        meta.getPersistentDataContainer().set(uuidKey, PersistentDataType.STRING, UUID.randomUUID().toString());

        item.setItemMeta(meta);
        return item;
    }

    /**
     * Registers your custom item.
     * @param item  Custom item to register
     * @apiNote If your items, blocks, or effects use your item in calls, make sure you register that item first before others.
     * @throws ItemRecipeException If something does not match with version given be the manager.
     * @throws IDPatternException If id pattern is wrong.
     */
    public static void register(@NotNull CustomItem item) {
        final Logger regLog = LoggerFactory.getLogger("CustomItemManager Register Process");
        String id = item.itemData().getId();

        Matcher matcher = instance.idPattern.matcher(id);

        if (!matcher.find())
            throw new IDPatternException(id + " is not matching pattern " + instance.idPattern.pattern());

        if (isItemBlocked(item)) {
            if (!Config.getConfig().getBoolean("items.loadBlockedItems")) {
                regLog.warn("Item with ID: " + id + " in blacklist. Skipping.");
                return;
            } else {
                regLog.warn("Item with ID: " + id + " in blacklist but still registering.");
            }
        }

        regLog.info("Adding recipe for " + id);
        NamespacedKey key = new NamespacedKey(Bukkit.getPluginManager().getPlugin("TailsLib"), id + "_recipe");
        ItemStack itemstack = instance.createRecipeItem(item);

        item.recipe(key, itemstack)
            .ifPresentOrElse((recipe) -> {
                if (!recipe.getResult().equals(itemstack)) {
                    regLog.error("End register process for item " + id);
                    regLog.error("Item failed recipe method.");
                    throw new IllegalStateException(String.format("Recipe MUST return item given " +
                    "in arguments but recipe returned something weird. Item will not be registered because of that. ", id));
                }
                if (item.itemData().isUniqueId())
                    regLog.warn("NOTICE: Custom items supports a UUID but UUID will appear after inventory click event on this item. Man i hate Bukkit Recipe API.");
                
                if (Bukkit.addRecipe(recipe))
                    regLog.info(String.format("Item recipe for %s is registered with NamespacedKey %s", id, key.asString()));
                else
                    regLog.info(String.format("Something went wrong while adding recipe for custom item with ID %s", id));
            }, () -> {
                regLog.info(String.format("Custom item with ID %s no recipe given.", id));
            });
        
        regLog.info("Validating item properties for " + id);

        for (CustomItemProperty prop : item.itemData().getProperties()) {
            String msg = prop.validateItem(item.itemData());

            if (msg != null) {
                regLog.error("End register process for item " + id);
                regLog.error("Valdation failed on " + prop.id());
                throw new IllegalStateException("CustomItemProperty failed to validate item. Fail message: " + msg);
            }
        }

        instance.items.put(id, item);

        regLog.info(id + " registered.");
    }


    /**
     * Blocks item from the server.
     * @param item Item to block
     */
    @SuppressWarnings("unchecked")
    public static void blockItem(@NotNull CustomItem item) {
        YamlConfiguration yaml = (YamlConfiguration) YAMLManager.require("TailsLib", "config.yml");
        List<String> blackList = (List<String>) yaml.getList("blacklistedItems");
        blackList.add(item.itemData().getId());
        yaml.set("blacklistedItems", blackList);
        Config.reloadConfig();
        log.info("Blocked item: " + item.itemData().getId());
    }

    /**
     * Unblocks item on server.
     * @param item Item ID to unblock
     */
    @SuppressWarnings("unchecked")
    public static void unblockItem(String item) {
        YamlConfiguration yaml = (YamlConfiguration) YAMLManager.require("TailsLib", "config.yml");
        List<String> blackList = (List<String>) yaml.getList("blacklistedItems");
        blackList.remove(item);
        yaml.set("blacklistedItems", blackList);
        Config.reloadConfig();
        log.info("Unblocked item: " + item);
    }

    /**
     * Blocks item from the server.
     * @param item Item ID to block
     */
    @SuppressWarnings("unchecked")
    public static void blockItem(@NotNull String item) {
        YamlConfiguration yaml = (YamlConfiguration) YAMLManager.require("TailsLib", "config.yml");
        List<String> blackList = (List<String>) yaml.getList("blacklistedItems");
        blackList.add(item);
        yaml.set("blacklistedItems", blackList);
        Config.reloadConfig();
        log.info("Blocked item: " + item);
    }

    /**
     * Unblocks item on server.
     * @param item Item to unblock
     */
    @SuppressWarnings("unchecked")
    public static void unblockItem(CustomItem item) {
        YamlConfiguration yaml = (YamlConfiguration) YAMLManager.require("TailsLib", "config.yml");
        List<String> blackList = (List<String>) yaml.getList("blacklistedItems");
        blackList.remove(item.itemData().getId());
        yaml.set("blacklistedItems", blackList);
        Config.reloadConfig();
        log.info("Blocked item: " + item.itemData().getId());
    }

    /**
     * Check if item is blocked.
     * @param item  Item to check.
     * @return      Is blocked or not.
     */
    public static boolean isItemBlocked(CustomItem item) {
        return instance.blacklistedItems.contains(item);
    }

    //
    //  * Papermc далбаёбы и не могут сделать нихуя нормально (:
    //  * Эта хуйня на Component не хочет делать новые линии
    //  * и заменяет их ебаным хер пойми чем.
    //  * 
    //  * Проще после такого на Spigot перейти и не ебаться.
    

    @SuppressWarnings({ "deprecation", "removal" })
    private ItemStack createRecipeItem(CustomItem item) {
        ItemStack itemstack = new ItemStack(item.itemData().getMaterial());
        ItemMeta meta = itemstack.getItemMeta();

        meta.displayName(Component.text(ChatColor.translateAlternateColorCodes('&', item.itemData().getName())));
        List<String> lore = new ArrayList<>();
        lore.addAll(item.itemData().getLore());
        meta.setLore(lore);
        if (item.customModelID() != 0)
            meta.setCustomModelData(item.customModelID());

        meta.getPersistentDataContainer().set(customItemKey, PersistentDataType.STRING, item.itemData().getId());
        itemstack.setItemMeta(meta);

        itemstack = item.improveItemStack(itemstack);

        return itemstack;
    }

    public static @Nullable CustomItem getItemByID(String id) {
        return getManager().items.get(id);
    }

}
