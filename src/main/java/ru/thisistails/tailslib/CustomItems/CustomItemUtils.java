package ru.thisistails.tailslib.CustomItems;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice.ExactChoice;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import net.kyori.adventure.text.Component;
import net.md_5.bungee.api.ChatColor;

/**
 * Utility class for CustomItemManager.
 */
public class CustomItemUtils {

    private CustomItemUtils() {}

    public enum CustomItemKeyStatus {
        Found,
        FoundButNotRegistered,
        NotFound
    }

    public static @NotNull CustomItemKeyStatus getKeyStatus(ItemStack item) {
        CustomItemManager manager = CustomItemManager.getManager();
        ItemMeta meta = item.getItemMeta();
        // Проверяем предмет на ключ и берём ID если есть
        if (meta != null && meta.getPersistentDataContainer().has(manager.getCustomItemKey(), PersistentDataType.STRING)) {
            String itemID = item.getItemMeta().getPersistentDataContainer().get(manager.getCustomItemKey(), PersistentDataType.STRING);
            CustomItem citem = manager.getItems().get(itemID);

            if (citem == null)
                return CustomItemKeyStatus.FoundButNotRegistered;
            else
                return CustomItemKeyStatus.Found;
        }

        return CustomItemKeyStatus.NotFound;
    }

    public static @Nullable UUID getUUIDFromCustomItem(ItemStack stack) {
        CustomItem citem = tryGetCItemFromItemStack(stack);
        if (citem == null || stack.getItemMeta() == null) return null;
        UUID uuid = null;

        try {
            uuid = UUID.fromString(stack.getItemMeta().getPersistentDataContainer().get(CustomItemManager.getManager().getUuidKey(), PersistentDataType.STRING));
        } catch (IllegalArgumentException | NullPointerException e) {}

        return uuid ;
    }

    /**
     * Tries to retrieve the identifier (ID) of a custom item from its ItemStack.
     * @param item The ItemStack from which the ID is to be retrieved.
     * @return The identifier (ID) of the custom item, or null if the ID is not found or the ItemStack does not contain metadata.
     */
    public static String tryGetIDFromItemStack(@NotNull ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        if (meta == null)
            return null;
        
        String id = item.getItemMeta().getPersistentDataContainer().get(CustomItemManager.getManager().getCustomItemKey(), PersistentDataType.STRING);
        return id;
    }

    /**
     * Tries to retrieve a custom item (CustomItem) from an ItemStack.
     * @param item The ItemStack for which to retrieve the CustomItem.
     * @return The CustomItem corresponding to the ItemStack, or null if the item is not custom.
     */
    public static @Nullable CustomItem tryGetCItemFromItemStack(ItemStack item) {
        if (item == null) return null;
        return CustomItemManager.getItemByID(tryGetIDFromItemStack(item));
    }

    /**
     * Gets the exact choice (ExactChoice) from the ID of a custom item.
     * @param id The identifier (ID) of the custom item.
     * @return The ExactChoice created from the custom item with the specified ID, or null if the item is not found.
     */
    public static @Nullable ExactChoice getExactChoiceFromID(@NotNull String id) {
        CustomItem item = CustomItemManager.getItemByID(id);
        if (item == null) return null;

        return new ExactChoice(createItem(item));
    }

    /**
     * Gets the exact choice (ExactChoice) from a custom item.
     * @param item The CustomItem for which to create the ExactChoice.
     * @return The ExactChoice created from the specified custom item, or null if the item is not found.
     */
    public static @Nullable ExactChoice getExactChoiceFromItem(@NotNull CustomItem item) {
        return getExactChoiceFromID(item.itemData().getId());
    }

    /**
     * Creates an ItemStack from the ID of a custom item.
     * @param id The identifier (ID) of the custom item.
     * @return The ItemStack created from the custom item with the specified ID, or null if the item is not found.
     */
    public static @Nullable ItemStack createItemFromID(@NotNull String id) {
        if (CustomItemManager.getItemByID(id) == null) return null;

        return createItem(CustomItemManager.getItemByID(id));
    }

    /**
     * Creates item from {@link CustomItem}.
     * @param item  {@link CustomItem}
     * @return      {@link ItemStack}
     */
    @SuppressWarnings({ "deprecation", "removal" })
    public static ItemStack createItem(@NotNull CustomItem item) {
        ItemStack itemstack = new ItemStack(item.itemData().getMaterial());
        ItemMeta meta = itemstack.getItemMeta();

        meta.displayName(Component.text(ChatColor.translateAlternateColorCodes('&', item.itemData().getName())));
        List<String> lore = new ArrayList<>();
        lore.addAll(item.itemData().getLore());
        meta.setLore(lore);
        if (item.customModelID() != 0)
            meta.setCustomModelData(item.customModelID());

            
        meta.getPersistentDataContainer().set(CustomItemManager.getManager().getCustomItemKey(), PersistentDataType.STRING, item.itemData().getId());
        itemstack.setItemMeta(meta);
            
        if (item.itemData().isUniqueId()) {
            itemstack = CustomItemManager.getManager().putUUID(itemstack);
        }

        itemstack = item.improveItemStack(itemstack);
        for (CustomItemProperty prop : item.itemData().getProperties())
            prop.itemCreated(itemstack, CustomItemUtils.getUUIDFromCustomItem(itemstack));

        return itemstack;
    }

}
