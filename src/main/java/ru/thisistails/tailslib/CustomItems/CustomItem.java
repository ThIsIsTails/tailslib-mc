package ru.thisistails.tailslib.CustomItems;

import java.util.Optional;
import java.util.UUID;

import org.bukkit.NamespacedKey;
import org.bukkit.event.Cancellable;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.RecipeChoice.ExactChoice;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.thisistails.tailslib.CustomItems.Data.CustomItemData;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomItems.CustomItemSwitchedEvent;

/**
 * Base class for all custom items.
 * Simple to use.
 */
public interface CustomItem {
    
    /**
     * Returns item data of this item.
     * @return  Custom Item Data
     * @implNote I do recommend to store item data and returning it in this method.
     */
    public @NotNull CustomItemData itemData();

    /**
     * Called when item has been created but still need additional setup.
     * <p>
     * Also called for {@link #recipe(NamespacedKey, ItemStack)} method, so
     * don't put some magic data or some data that can be randomized, or else
     * every item created with recipe gonna have same data every time. Use
     * {@link #itemCreation(CraftItemEvent)} event for magic, random data.
     * </p>
     * <p>
     * Use this method for setuping attributed, max durability and etc.
     * </p>
     * @param current   Item.
     * @apiNote         Item alreade have description, name and ID built-in.
     */
    public default @NotNull ItemStack improveItemStack(@NotNull ItemStack current) { return current; }

    /**
     * Indentifies that u want use your own model for this item.
     * <p>You need to make your own texturepack for this function or else nothing will happen.</p>
     * @return Custom model ID for client.
     * @deprecated Custom model id can be setup via {@link ItemMeta}.
     * <p>
     * Use {@link #improveItemStack(ItemStack)} to add custom model instead.
     * </p>
     */
    @Deprecated(forRemoval = true, since = "1.8-beta")
    public default int customModelID() { return 0; }

    /**
     * Called when human like entity tries to craft this item.
     * <p>
     * Can be used to change default display name specified in item data or else.
     * </p>
     * @param event
     * @apiNote If use {@link CustomItemData#isUniqueId()} do not try to get UUID. UUID is not generated before this event.
     */
    public default void itemCreation(CraftItemEvent event) {}

    /**
     * Left click event for your item. 
     * <p>
     * It will not fire if {@link #itemDamageEntity(EntityDamageByEntityEvent, UUID)} is called
     * or if original event was cancelled by item property.
     * </p>
     * @param event Event
     * @param uuid  Item UUID if {@link CustomItemData#isUniqueId()} is true or else it will be null.
     */
    public default void leftClick(@NotNull PlayerInteractEvent event, @Nullable UUID uuid) {}
    
    /**
     * Right click event for your item.
     * <p>
     * It will not fire if original event was cancelled by item property or {@link #rightClickOnEntity(PlayerInteractEntityEvent, UUID)} was called.
     * </p>
     * @param event Event
     * @param uuid Item UUID if {@link CustomItemData#isUniqueId()} is true or else it will be null.
     */
    public default void rightClick(@NotNull PlayerInteractEvent event, @Nullable UUID uuid) {}

    /**
     * Called when player damage entity with this item.
     * <p>This event will not fire if original event was cancelled after calling item properties.</p>
     * @param event Event
     * @param uuid  Item UUID if {@link CustomItemData#isUniqueId()} is true or else it will be null.
     */
    public default void itemDamageEntity(@NotNull EntityDamageByEntityEvent event, @Nullable UUID uuid) {}

    /**
     * Right click on entity with this item.
     * <p>
     * It will not fire if original event was cancelled by item property.
     * </p>
     * @param event Event
     * @param uuid  Item UUID if {@link CustomItemData#isUniqueId()} is true or else it will be null.
     */
    public default void rightClickOnEntity(@NotNull PlayerInteractEntityEvent event, @Nullable UUID uuid) {}

    /**
     * Recipe for your item.
     * @param key   NamespacedKey for recipe constructor
     * @param item  Itemstack of your item for recipe constructor.
     * @return  Recipe
     * @apiNote Use item and key in constructon or else manager will throw {@link IllegalStateException}.
     * @implNote Use {@link CustomItemUtils#getExactChoiceFromID(String)} or {@link CustomItemUtils#getExactChoiceFromItem(CustomItem)} to get {@link ExactChoice}.
     */
    public default Optional<Recipe> recipe(NamespacedKey key, ItemStack item) { return Optional.empty(); }
    
    /**
     * Will be called when player will select this item.
     * <p>
     * UUID of this item can be fetched via {@link CustomItemUtils#getUUIDFromCustomItem(ItemStack)}.
     * </p>
     * @param event Event
     * @implNote This method is gonna call after {@link #itemSwitchEvent(CustomItemSwitchedEvent)}
    */
    public default void itemSelectEvent(CustomItemSwitchedEvent event) {}

    /**
     * Called when player has switched this item to another.
     * <p>
     * UUID of this item can be fetched via {@link CustomItemUtils#getUUIDFromCustomItem(ItemStack)}.
     * </p>
     * @param event Event
     */
    public default void itemSwitchEvent(CustomItemSwitchedEvent event) {}

    /**
     * Called when item is being dropped to the ground by player.
     * @param event Bukkit event
     * @param uuid UUID if presented
     */
    public default void itemDroppedEvent(PlayerDropItemEvent event, @Nullable UUID uuid) {}

    /**
     * Called when item is blocking recipe because {@link CustomItemData#isUniqueMaterial()} enabled.
     * <p>
     * Set {@link Cancellable#setCancelled(boolean)} to false to allow craft.
     * </p>
     * @param event Event
     * @param uuid UUID if presented
     */
    public default void itemBlockedInCraftEvent(PrepareItemCraftEvent event, @Nullable UUID uuid) {}

    /**
     * Called when inventory opened with this item in inventory.
     * @param event Bukkit event.
     * @param item  Founded item.
     * @apiNote Will not fire when player opens his inventory because its impossible to track when player opens his inventory.
     */
    public default void inventoryOpened(InventoryOpenEvent event, ItemStack item) {}

    /**
     * Called when inventory opened with this item in inventory.
     * <p>
     * This inventory is made by plugin and changing the inventory can be a bad idea.
     * </p>
     * @param event Bukkit event.
     * @param item  Founded item.
     * @apiNote Can do mistakes cuz sometimes people creating inventories with player holder.
     */
    public default void customInventoryOpened(InventoryOpenEvent event, ItemStack item) {}

}
