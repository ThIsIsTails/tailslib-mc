package ru.thisistails.tailslib.CustomEffects;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import ru.thisistails.tailslib.Localization;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;
import ru.thisistails.tailslib.CustomEffects.GsonAdapters.AppliedEffectsSer;
import ru.thisistails.tailslib.CustomEffects.GsonAdapters.CEffectJsonData;
import ru.thisistails.tailslib.Exceptions.IDPatternException;
import ru.thisistails.tailslib.Tools.Config;
import ru.thisistails.tailslib.Tools.Debug;
import ru.thisistails.tailslib.Utils.TLibListeners.LibListener;
import ru.thisistails.tailslib.Utils.TLibListeners.TailsLibListeners;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectAppliedEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectEndEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectInterruptionEvent;

@Slf4j
public class CustomEffectManager {
    private static @Getter boolean sendMessagesToPlayers;

    static {
        FileConfiguration yaml = Config.getConfig();
        instance = new CustomEffectManager();
        sendMessagesToPlayers = yaml.getBoolean("effects.sendMessages");
    }
    
    protected HashSet<CustomEffect> registeredEffects = new HashSet<>();
    protected List<AppliedEffectData> appliedEffects = new ArrayList<>();
    public static final Pattern idPattern = Pattern.compile("^[a-z_]+$");
    public static final String saveFilePath = Bukkit.getPluginManager().getPlugin("TailsLib").getDataFolder().getAbsolutePath() + "/appliedEffectsData.json";

    private static CustomEffectManager instance;

    private CustomEffectManager() {}

    public static CustomEffectManager getInstance() {
        return instance;
    }

    /**
     * Returns a copy of a hash set with all registered effects.
     * @return  Copy of hash set with registered effects.
     */
    public static Set<CustomEffect> getRegisteredEffects() {
        return new HashSet<>(instance.registeredEffects);
    }

    /**
     * Returns a copy of a array list with all applied effects.
     * @return  Copy of a array list with applied effects.
     */
    public static List<AppliedEffectData> getAppliedEffects() {
        return new ArrayList<>(instance.appliedEffects);
    }

    /**
     * Loads effects
     */
    public void loadEffects() {
        Gson gson = new GsonBuilder().setPrettyPrinting()
            .excludeFieldsWithoutExposeAnnotation()
            .registerTypeAdapter(CEffectJsonData.class, new AppliedEffectsSer())
            .create();

        try {
            String json = readFromInputStream(new FileInputStream(new File(saveFilePath)));
            appliedEffects.addAll(gson.fromJson(json, CEffectJsonData.class).getData());
            Debug.info(json);
        } catch (IOException error) {
            error.printStackTrace();
        }
    }

    /**
     * Triggers TailsLib to save all applied effect on this moment.
     * Do not use it if you dont understand what are you doing! It may cause some bugs after restart.
     * <p>Basicly it will trigger again when server will disabling or restarting.</p>
     */
    public void saveEffects() {
        try {
            File file = new File(saveFilePath);
            if (!file.exists()) {
                if (!file.createNewFile()) 
                    log.warn("Failed to create save file: " + file.getPath());
            }

            save();
        } catch (IOException e) {
            log.error("An error occurred while saving applied effects data: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void save() throws IOException {
        Debug.info("Saving effects...");
        Gson gson = new GsonBuilder().setPrettyPrinting()
        .excludeFieldsWithoutExposeAnnotation()
        .registerTypeAdapter(CEffectJsonData.class, new AppliedEffectsSer())
        .create();

        String json = gson.toJson(new CEffectJsonData(appliedEffects));
        Debug.info("Generated JSON: " + json);

        File file = new File(saveFilePath);

        if (!file.exists()) file.createNewFile();

        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(json);
        writer.close();
    }

    private static String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
        = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

    /**
     * Registers the effect.
     * @param effect    Effect to register
     * @apiNote If your items, blocks, or effects use your effect in calls, make sure you register that effect first before others.
     */
    public static void register(@NotNull CustomEffect effect) {
        if (isEffectRegistered(effect)) {
            log.warn("Effect with ID " + effect.effectData().getId() + " already registered. Skipping...");
            return;
        }

        String id = effect.effectData().getId();

        Matcher matcher = idPattern.matcher(id);

        if (!matcher.find())
            throw new IDPatternException(id + " is not matching pattern " + idPattern.pattern());

        instance.registeredEffects.add(effect);
        log.info("Custom effect with ID " + effect.effectData().getId() + " registered.");
    }

    /**
     * Returns registered custom effect or null.
     * @param id    ID of custom effect
     * @return      Registered custom effect or null.
     */
    public static @Nullable CustomEffect getEffectByID(@NotNull String id) {
        for (CustomEffect effect : getInstance().registeredEffects)
            if (effect.effectData().getId().equals(id))
                return effect;
        
        return null;
    }

    protected void decreaseDur(AppliedEffectData d, int amount) {
        appliedEffects.forEach((data) -> {
            if (data.getUuid().equals(d.getUuid())) {
                data.setDuration(data.getDuration() - amount);
            }
        });
    }

    /**
     * Returns applied effect data on player.
     * @param player    Player to check
     * @param ceffect    Effect to get
     * @return  {@link AppliedEffectData} or null if player doesn't have this effect on him.
     */
    public static @Nullable AppliedEffectData getAppliedEffectData(@NotNull Player player, @NotNull CustomEffect ceffect) {
        for (AppliedEffectData data : getInstance().appliedEffects) {
            if (data.getClass().equals(ceffect.getClass()))
                return data;
        }

        return null;
    }

    /**
     * Checking if given effect is registered.
     * @param effect    Effect
     * @return          Is registered?
     */
    public static boolean isEffectRegistered(CustomEffect effect) {
        return getEffectByID(effect.effectData().getId()) != null;
    }

    /**
     * Checking if given effect is registered.
     * @param id        ID of effect
     * @return          Is registered?
     */
    public static boolean isEffectRegistered(String id) {
        return getEffectByID(id) != null;
    }

    /**
     * Checks if player have specific effect on him.
     * <p>This function do not checks level or duration of the effect.</p>
     * @param player    Player
     * @param effect    Effect
     * @return  True if player have specific effect or false if doesn't
     */
    public static boolean containEffect(@NotNull Player player, @NotNull CustomEffect effect) {
        return getAppliedEffectData(player, effect) != null;
    }

    /**
     * Returns all applied effects on player in this current tick.
     * @param player Player
     * @return  Applied effects
     */
    public static List<AppliedEffectData> getAllPlayerAppliedEffects(@NotNull Player player) {
        List<AppliedEffectData> list = new ArrayList<>();

        for (AppliedEffectData data : getInstance().appliedEffects) {
            if (data.getOfflinePlayer().getUniqueId().equals(player.getUniqueId()))
                list.add(data);
        }

        return list;
    }

    /**
     * This function returns all {@link AppliedEffectData}'s of effect
     * @param effect    Effect
     * @return          All {@link AppliedEffectData}'s of effect
     */
    public static List<AppliedEffectData> getAllDatasWithEffect(@NotNull CustomEffect effect) {
        List<AppliedEffectData> list = new ArrayList<>();

        for (AppliedEffectData data : getInstance().appliedEffects) {
            if (data.getEffect() == effect)
                list.add(data);
        }

        return list;
    }


    protected void effectEnded(AppliedEffectData data) {
        if (data.getDuration() == 0) {
            appliedEffects.remove(data);
            CEffectEndEvent event = new CEffectEndEvent(data);
            data.getEffect().effectEnd(event);
            for (LibListener listeners : TailsLibListeners.getListeners()) {
                try {
                    listeners.onEffectEnded(event);
                } catch (Exception error) {
                    log.error("Failed to execute listener on effect ended event: " + listeners.getClass().getName(), error);
                }
            }

            if (sendMessagesToPlayers) {
                data.getPlayer().getPlayer().sendMessage(Localization.prefix + " " + Localization.onEffectEnd
                    .replace("%effect_name%", data.getEffect().effectData().getName())
                    .replace("%effect_id%", data.getEffect().effectData().getId())
                    .replace("%effect_duration%", String.valueOf(data.getDuration()))
                    .replace("%effect_level%", String.valueOf(data.getLevel()))
                );
            }
        }
    }

    protected boolean callEffect(@NotNull AppliedEffectData data) {
        if (data.getLevel() > data.getEffect().effectData().getMaxLevel()) {
            Bukkit.getLogger().severe("[CustomEffectManager] Something went wrong while executing #callEffect. [" + data.getEffect().effectData().getId() + 
            "; player: " + data.getPlayer().getName() + "]");
            Bukkit.getLogger().severe("[CustomEffectManager] AppliedEffectData have more level than max level allowed.");
            Bukkit.getLogger().severe("[CustomEffectManager] Effect will be interrupted with ErrorOccured reason.");
            interruptEffect(data.getPlayer(), data.getEffect(), EffectInterruptionReason.ErrorOccured);
        }

        try {
            data.getEffect().process(data);
            decreaseDur(data, 1);

            if (data.getDuration() == 0) {
                effectEnded(data);
            }
            return true;
        } catch (Exception error) {
            error.printStackTrace();
            Bukkit.getLogger().severe("Effect " + data.getEffect().effectData().getId() + " has occured an exception.");
            if (data.getPlayer() != null)
                Debug.error(data.getPlayer().getPlayer(), Localization.onEffectInterruption
                    .replace("%effect_name%", data.getEffect().effectData().getName())
                    .replace("%effect_id%", data.getEffect().effectData().getId())
                    .replace("%effect_duration%", String.valueOf(data.getDuration()))
                    .replace("%effect_level%", String.valueOf(data.getLevel()))
                    );
            return false;
        }
    }

    /**
     * Applies effect to player.
     * @param player    Player
     * @param effect    Custom effect (Must be registered via {@link #register(CustomEffect)})
     * @param level     Level of the effect
     * @param duration  Duration of the effect in seconds. (Not ticks)
     * @return          Success, if not then it means that custom effect have errors or one of listeners has cancelled this.
     */
    public static boolean applyEffect(@NotNull Player player, @NotNull CustomEffect effect, @NotNull int level, @NotNull int duration) {

        AppliedEffectData data = new AppliedEffectData(effect, player.getUniqueId(), level, duration);
        
        // Для проверки
        AppliedEffectData check = getAppliedEffectData(player, effect);

        CEffectAppliedEvent cevent = new CEffectAppliedEvent(data, check);

        for (LibListener listeners : TailsLibListeners.getListeners()) {
            try {
                listeners.onEffectApplied(cevent);
            } catch (Exception error) {
                log.error("Failed to execute listener on effect apply event: " + listeners.getClass().getName(), error);
            }
        }

        if (cevent.isCancelled()) return false;
        
        if (check != null) {
            if (data.getLevel() > check.getLevel() || data.getDuration() > check.getDuration())
            interruptEffect(player, effect, EffectInterruptionReason.ApplyingHigherLevelEffect);
        }
        

        try {
            data.getEffect().effectApplied(data);
            if (sendMessagesToPlayers && data.getPlayer() != null) {
                data.getPlayer().sendMessage(Localization.prefix + " " + Localization.onEffectApply
                    .replace("%effect_name%", data.getEffect().effectData().getName())
                    .replace("%effect_id%", data.getEffect().effectData().getId())
                    .replace("%effect_duration%", String.valueOf(data.getDuration()))
                    .replace("%effect_level%", String.valueOf(data.getLevel()))
                );
            }
        } catch (Exception error) {
            CEffectInterruptionEvent event = new CEffectInterruptionEvent(data, EffectInterruptionReason.ErrorOccured);
            data.getEffect().effectInterrupted(event);
            log.error("Effect " + data.getEffect().effectData().getId() + " has occured an exception.", error);
            return false;
        }

        instance.appliedEffects.add(data);

        return true;
    }

    /**
     * Interrupt effect AKA remove it.
     * <p>This operation will trigger {@link CustomEffect#onEffectInterrupted(Player, int, EffectInterruptionReason)}</p>
     * @param player    Player to remove effect from
     * @param effect    Effect to remove.
     */
    public static void interruptEffect(Player player, CustomEffect effect, EffectInterruptionReason reason) {
        AppliedEffectData data = getAppliedEffectData(player, effect);

        if (data == null)
            return;
        
        instance.appliedEffects.remove(data);
        CEffectInterruptionEvent event = new CEffectInterruptionEvent(data, reason);

        for (LibListener listeners : TailsLibListeners.getListeners()) {
            try {
                listeners.onEffectInterruption(event);
            } catch (Exception error) {
                log.error("Failed to execute listener on effect interruption event: " + listeners.getClass().getName(), error);
            }
        }

        try {
            effect.effectInterrupted(event);
        } catch (Exception error) {
            log.error("Effect: " + effect.effectData().getId() + " occured an error.", error);
        }
    }

}
