package ru.thisistails.tailslib.CustomEffects.Data;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.ApiStatus.Internal;

import com.google.gson.annotations.Expose;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import ru.thisistails.tailslib.CustomEffects.CustomEffect;

/**
 * Data class for applied effects.
 */
@Data
public class AppliedEffectData {

    public AppliedEffectData(@NotNull CustomEffect effect, @NotNull UUID playerUuid, @NotNull int level, @NotNull int duration) {
        this.effect = effect;
        this.entityUuid = playerUuid;
        this.level = level;
        this.duration = duration;
        this.uuid = UUID.randomUUID();
    }

    public AppliedEffectData(@NotNull CustomEffect effect, @NotNull OfflinePlayer offlinePlayer, @NotNull int level, @NotNull int duration) {
        this.effect = effect;
        this.entityUuid = offlinePlayer.getUniqueId();
        this.level = level;
        this.duration = duration;
        this.uuid = UUID.randomUUID();
    }

    public AppliedEffectData(@NotNull CustomEffect effect, @NotNull Player player, @NotNull int level, @NotNull int duration) {
        this.effect = effect;
        this.entityUuid = player.getUniqueId();
        this.level = level;
        this.duration = duration;
        this.uuid = UUID.randomUUID();
    }
    
    private @Setter(AccessLevel.NONE) UUID uuid;
    /**
     * Applied effect
     */
    @Expose
    private @Setter(AccessLevel.NONE) @NotNull CustomEffect effect;

    /**
     * Player that have this effect
     */
    @Expose
    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private @NotNull UUID entityUuid;
    /**
     * Level of the effect.
     * @apiNote Level must be more than 1 or equals to 1 + {@link CustomEffectData#getMaxAmplifier()} or else on tick calls manager will interrupt effect.
     */
    @Expose
    @Setter(AccessLevel.NONE)
    private int level;

    @Expose
    @Setter(AccessLevel.PUBLIC)
    @Internal
    private int duration;

    /**
     * Returns offline player.
     * @return  {@link OfflinePlayer}
     */
    public OfflinePlayer getOfflinePlayer() {
        return Bukkit.getOfflinePlayer(entityUuid);
    }

    /**
     * Returns possible online player.
     * @return {@link Player}
     */
    public @Nullable Player getPlayer() {
        return getOfflinePlayer().getPlayer();
    }

}
