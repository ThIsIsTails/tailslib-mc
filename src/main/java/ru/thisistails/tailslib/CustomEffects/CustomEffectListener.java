package ru.thisistails.tailslib.CustomEffects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerLoadEvent;
import org.bukkit.inventory.ItemStack;

import lombok.extern.slf4j.Slf4j;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;
import ru.thisistails.tailslib.Utils.TLibListeners.LibListener;
import ru.thisistails.tailslib.Utils.TLibListeners.TailsLibListeners;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectQuitProcessEvent;

@Slf4j
public class CustomEffectListener implements Listener {
    private static CustomEffectListener instance;

    private CustomEffectListener() {}

    public static CustomEffectListener getListener() {
        if (instance == null) instance = new CustomEffectListener();

        return instance;
    }

    @EventHandler
    public void onServerLoad(ServerLoadEvent event) {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("TailsLib"), new Runnable() {
            private int ticks = 0;

            @Override
            public void run() {
                ticks++;

                for (AppliedEffectData data : CustomEffectManager.getAppliedEffects()) {
                    try {
                        data.getEffect().processTick(data);
                     } catch (Exception e) {
                         log.error("Failed to process tick for " + data.getEffect().effectData().getId() + ".", e);
                         CustomEffectManager.interruptEffect(data.getPlayer(), data.getEffect(), EffectInterruptionReason.ErrorOccured);
                         continue;
                     }
                }

                if (ticks == 20) {
                    ticks = 0;
                    processTick();
                }

            }
            
        }, 0, 1);

        log.info("CustomEffects scheduler task started.");
    }

    private void processTick() {
        List<AppliedEffectData> toRemove = new ArrayList<>();
        List<AppliedEffectData> appliedEffects = CustomEffectManager.getAppliedEffects();
        for (AppliedEffectData data : appliedEffects) {
            if (data.getPlayer() == null) {
                // Skipping offline players
                continue;
            }

            if (data.getDuration() <= 0) {
                if (!toRemove.contains(data))
                    toRemove.add(data);
                else
                    continue;
            }

            for (LibListener listeners : TailsLibListeners.getListeners()) {
                 try {
                     listeners.onEffectProcess(data);
                 } catch (Exception e) {
                     log.error("Failed to execute listener event.", e);
                 }
            }

            if (!CustomEffectManager.getInstance().callEffect(data)) {
                CustomEffectManager.interruptEffect(data.getPlayer(), data.getEffect(), EffectInterruptionReason.ErrorOccured);
            }


        }

        for (AppliedEffectData data : toRemove)
            CustomEffectManager.getInstance().effectEnded(data);
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        for (AppliedEffectData data : CustomEffectManager.getAllPlayerAppliedEffects(player)) {
            try {
                CEffectQuitProcessEvent processEvent = new CEffectQuitProcessEvent(data, true);
                data.getEffect().playerQuitWhileAppliedEffect(processEvent);

                for (LibListener listeners : TailsLibListeners.getListeners()) {
                    try {
                        listeners.onEffectPlayerQuitProcess(processEvent);
                    } catch (Exception error) {
                        log.error("Failed to execute listener on player quit event: " + listeners.getClass().getName(), error);
                    }
                }

                if (!processEvent.isSaveEffect())
                    CustomEffectManager.getInstance().appliedEffects.remove(data);
            } catch (Exception error) {
                log.error("Error while executing playerQuitWhileAppliedEffect on " + data.getEffect().effectData().getId(), error);
            }
        }

        CustomEffectManager.getInstance().saveEffects();
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getPlayer();
        CustomEffectManager.getAllPlayerAppliedEffects(player).forEach((data) -> {
            if (data.getEffect().effectData().isClearOnDeath())
                CustomEffectManager.interruptEffect(player, data.getEffect(), EffectInterruptionReason.PlayerDied);
        });
    }

    
    @EventHandler
    public void onMilkDrink(PlayerItemConsumeEvent event) {
        Player player = event.getPlayer();
        ItemStack item = event.getItem();

        if (item.getType() == Material.MILK_BUCKET) {
            CustomEffectManager.getAllPlayerAppliedEffects(player).forEach((data) -> {
                if (data.getEffect().effectData().isCanBeClearedByMilk())
                    CustomEffectManager.interruptEffect(player, data.getEffect(), EffectInterruptionReason.PlayerDrinksMilk);
            });
        }
    }

}
