package ru.thisistails.tailslib.CustomEffects;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.ApiStatus.Experimental;

import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;
import ru.thisistails.tailslib.CustomEffects.Data.CustomEffectData;
import ru.thisistails.tailslib.Utils.TLibListeners.LibListener;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectEndEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectInterruptionEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectQuitProcessEvent;

/**
 * Basic interface for Custom Effects
 */
public interface CustomEffect {

    /**
    *   Returns basic for this effects
    *   @return Basic data of this effect
    */
    public @NotNull CustomEffectData effectData();

    /**
     * Sets maximum duration of this effect.
     * Called on applying.
     * @param level Level
     * @param player Player
     * @return      Max duration; Set to -1 to make infinite.
     */
    public default int maxDuration(Player player, int level) { return -1; }

    /**
     * Called every second while this effect is applied.
     * <p>
     * If this event give an error then this effect will be interrupted with reason {@link EffectInterruptionReason#ErrorOccured}
     * </p>
     * @param data Applied effect data.
     * @see AppliedEffectData
     */
    public default void process(AppliedEffectData data) {}

    /**
     * Called every tick while this effect is applied.
     * <p>
     * This method is basicly is experimental.
     * I don't really know how this may affect TPS so it's just a test.
     * </p>
     * <p>
     * Effect duration still will be counted in seconds.
     * </p>
     * @param data  Applied effect data.
     * @apiNote This method has no event in {@link LibListener}.
     */
    @Experimental
    public default void processTick(AppliedEffectData data) {}

    /**
     * Called when the effect ends on its own and has not been interrupted.
     * @param event Event
     */
    public default void effectEnd(CEffectEndEvent event) {}

    /**
     * Called when the effect has been interrupted for any reason.
     * @param event Event
     * @see EffectInterruptionReason
     */
    public default void effectInterrupted(CEffectInterruptionEvent event) {}

    /**
     * Called when the effect has been applied.
     * @param data  Applied effect data
     * @apiNote If this event give an error then this effect will be interrupted with reason {@link EffectInterruptionReason#ErrorOccured}
     */
    public default void effectApplied(AppliedEffectData data) {}

    /**
     * Calls when player quit while this applied on him.
     * @param event Event
     */
    public default void playerQuitWhileAppliedEffect(CEffectQuitProcessEvent event) {}


}
