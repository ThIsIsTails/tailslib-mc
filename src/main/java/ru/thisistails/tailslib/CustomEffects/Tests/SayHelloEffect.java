package ru.thisistails.tailslib.CustomEffects.Tests;

import org.jetbrains.annotations.NotNull;

import ru.thisistails.tailslib.CustomEffects.CustomEffect;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;
import ru.thisistails.tailslib.CustomEffects.Data.CustomEffectData;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectEndEvent;
import ru.thisistails.tailslib.Utils.TLibListeners.Events.CustomEffect.CEffectInterruptionEvent;

public class SayHelloEffect implements CustomEffect {

    @Override
    public @NotNull CustomEffectData effectData() {
        return new CustomEffectData("sayhelloeffect", "Say hello effect!", 0, false, true);
    }

    @Override
    public void effectEnd(CEffectEndEvent event) {
        event.getData().getPlayer().sendMessage("Goodbye!");
    }

    @Override
    public void effectInterrupted(CEffectInterruptionEvent event) {
        event.getData().getPlayer().sendMessage("Effect has been interrupted because of " + event.getReason());
    }

    @Override
    public void process(AppliedEffectData data) {
        data.getPlayer().sendMessage(String.valueOf(data.getDuration()));
    }
}
