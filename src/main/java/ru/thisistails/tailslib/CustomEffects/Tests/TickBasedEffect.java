package ru.thisistails.tailslib.CustomEffects.Tests;

import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import ru.thisistails.tailslib.CustomEffects.CustomEffect;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;
import ru.thisistails.tailslib.CustomEffects.Data.CustomEffectData;
import ru.thisistails.tailslib.Utils.Cooldown;
import ru.thisistails.tailslib.Utils.CooldownTask;

public class TickBasedEffect implements CustomEffect {

    private Cooldown<Player> cooldown = new Cooldown<>("TailsLib");

    @Override
    public @NotNull CustomEffectData effectData() {
        return new CustomEffectData("tickbasedeffect", "Effect that based on ticks", 0, false, false);
    }

    @Override
    public void effectApplied(AppliedEffectData data) {
        data.getPlayer().sendMessage("This effect is based on ticks, not seconds.");
        cooldown.addCooldownTask(data.getPlayer(), 10 * 20);
    }

    @Override
    public void processTick(AppliedEffectData data) {
        data.getPlayer().sendMessage("Wow, a tick! Random number: " + ThreadLocalRandom.current().nextInt(9999));
        CooldownTask task = (CooldownTask) cooldown.getCooldowns().get(data.getPlayer());
        double leftTicks = task.getLeftTime();
        data.getPlayer().sendMessage("Cooldown remaining: " + leftTicks / 20);
    }
    
}
