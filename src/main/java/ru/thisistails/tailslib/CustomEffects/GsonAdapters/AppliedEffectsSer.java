package ru.thisistails.tailslib.CustomEffects.GsonAdapters;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import ru.thisistails.tailslib.CustomEffects.CustomEffect;
import ru.thisistails.tailslib.CustomEffects.CustomEffectManager;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;
import ru.thisistails.tailslib.Tools.Debug;

public class AppliedEffectsSer implements JsonDeserializer<CEffectJsonData>, JsonSerializer<CEffectJsonData> {

    @Override
    public JsonElement serialize(CEffectJsonData src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject element = new JsonObject();
        JsonArray array = new JsonArray();

        for (AppliedEffectData effect : src.getData()) {
            Debug.info("Serializing " + effect.getEffect().effectData().getId() + " for " + effect.getOfflinePlayer().getUniqueId().toString());
            
            JsonObject obj = new JsonObject();
            obj.addProperty("effectId", effect.getEffect().effectData().getId());
            obj.addProperty("playerUuid", effect.getOfflinePlayer().getUniqueId().toString());
            obj.addProperty("level", effect.getLevel());
            obj.addProperty("duration", effect.getDuration());

            array.add(obj);
        }

        element.addProperty("_comment", "Do not change anything here if you don't know what are you doing!");
        element.addProperty("_comment2", "It's your problem if something goes wrong after your changes.");
        element.add("AppliedEffects", array);
        Debug.info(element.toString());

        return element;
    }

    @Override
    public CEffectJsonData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<AppliedEffectData> data = new ArrayList<>();
        /*
         
        {
            "AppliedEffects": [
                {
                    "effectId": "helloeffect",
                    "playerUuid": "uuid",
                    "level": 5,
                    "duration": 20
                }
            ]
        }

         */

        json.getAsJsonObject().get("AppliedEffects").getAsJsonArray()
            .iterator().forEachRemaining((element) -> {
                JsonObject obj = element.getAsJsonObject();

                UUID playerUuid = UUID.fromString(obj.get("playerUuid").getAsString());
                String effectId = obj.get("effectId").getAsString();
                int level = obj.get("level").getAsInt();
                int duration = obj.get("duration").getAsInt();

                if (CustomEffectManager.getEffectByID(effectId) == null) {
                    Debug.error("Error while loading effects. No effect with ID " + effectId);
                    return;
                }

                OfflinePlayer player = Bukkit.getOfflinePlayer(playerUuid);
                CustomEffect effect = CustomEffectManager.getEffectByID(effectId);
                AppliedEffectData appliedEffectData = new AppliedEffectData(effect, player, level, duration);

                // Not safe but who gonna use this class exept me?
                data.add(appliedEffectData);
            });

        return new CEffectJsonData(data);
    }
    
}
