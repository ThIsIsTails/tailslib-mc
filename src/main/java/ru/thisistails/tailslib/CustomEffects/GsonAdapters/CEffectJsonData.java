package ru.thisistails.tailslib.CustomEffects.GsonAdapters;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import ru.thisistails.tailslib.CustomEffects.Data.AppliedEffectData;

public class CEffectJsonData {

    @Getter
    @Setter
    private List<AppliedEffectData> data;
    
    public CEffectJsonData(List<AppliedEffectData> data) {
        this.data = data;
    }
    
}
