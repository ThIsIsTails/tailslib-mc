package ru.thisistails.tailslib.Listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerLoadEvent;
import org.bukkit.scheduler.BukkitRunnable;

import ru.thisistails.tailslib.CustomBlocks.CustomBlockManager;
import ru.thisistails.tailslib.CustomEffects.CustomEffectManager;
import ru.thisistails.tailslib.Tools.Config;

public class StartLoadingAndSavingListener implements Listener {

    @EventHandler
    public void onServerLoad(ServerLoadEvent event) {
        Bukkit.getLogger().info("[TailsLib] Server load event recieved. Starting loading process.");

        CustomBlockManager.getInstance().loadBlocks();
        CustomEffectManager.getInstance().loadEffects();

        if (Config.getConfig().getBoolean("autosave.enabled")) {
            int time = Config.getConfig().getInt("autosave.time") * 20;

            new BukkitRunnable() {

                @Override
                public void run() {
                    Bukkit.getLogger().info("[TailsLib : Autosaving] Calling save functions.");
                    CustomEffectManager.getInstance().saveEffects();
                    CustomBlockManager.getInstance().savePlacedBlocksDatas();
                }
                
            }.runTaskTimer(Bukkit.getPluginManager().getPlugin("TailsLib"), time, time);

            Bukkit.getLogger().info("[TailsLib] Auto save enabled and started.");
        }
    }
    
}
