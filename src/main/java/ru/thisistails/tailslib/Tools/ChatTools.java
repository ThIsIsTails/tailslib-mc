package ru.thisistails.tailslib.Tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.Getter;
import net.kyori.adventure.text.Component;
import net.md_5.bungee.api.ChatColor;

public class ChatTools {

    private static @Getter Pattern pattern = Pattern.compile("#[a-fA-F0-9]{6}");

    /**
     * Return message with replcaed color codes.
     * @param message   Message
     * @return          String with color
     * @apiNote         Supports HEX codes
     * @see YAMLManager#getAndTranslateString
     */
    public static String translateString(String message) {
        Matcher match = pattern.matcher(message);
        while (match.find()) {
            String color = message.substring(match.start(), match.end());
            message = message.replace(color, ChatColor.of(color).toString());
            match = pattern.matcher(message);
        }
        message = ChatColor.translateAlternateColorCodes('&', message);

        return message;
    }

    /**
     * Simply displaying message to all players on server.
     * @param message   Message
     * @apiNote Color codes already translated via {@link #translateString(String)}
     */
    public static void sendAll(String message) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage(translateString(message));
        }
    }

    /**
     * Simply displaying message to all players on server.
     * @param message   Message
     * @apiNote You must translate color codes by yourself.
     */
    public static void sendAll(Component message) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage(message);
        }
    }

    /**
     * Simply displaying message to all players on server.
     * @param message       Message
     * @param exceptPlayers Who will not recieve your messsage
     * @apiNote Color codes already translated via {@link #translateString(String)}
     */
    public static void sendAll(String message, Player... exceptPlayers) {
        List<Player> exceptList = List.of(exceptPlayers);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (exceptList.contains(player)) continue;
            player.sendMessage(translateString(message));
        }
    }

    /**
     * Simply displaying message to all players on server.
     * @param message       Message
     * @param exceptPlayers Who will not recieve your messsage
     * @apiNote Color codes already translated via {@link #translateString(String)}
     */
    public static void sendAll(String message, Collection<Player> exceptPlayers) {
        List<Player> exceptList = new ArrayList<>(exceptPlayers);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (exceptList.contains(player)) continue;
            player.sendMessage(translateString(message));
        }
    }

    /**
     * Simply displaying message to all players on server.
     * @param message       Message
     * @param exceptPlayers Who will not recieve your messsage
     * @apiNote You must translate color codes by yourself.
     */
    public static void sendAll(Component message, Player... exceptPlayers) {
        List<Player> exceptList = List.of(exceptPlayers);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (exceptList.contains(player)) continue;
            player.sendMessage(message);
        }
    }

    /**
     * Simply displaying message to all players on server.
     * @param message       Message
     * @param exceptPlayers Who will not recieve your messsage
     * @apiNote You must translate color codes by yourself.
     */
    public static void sendAll(Component message, Collection<Player> exceptPlayers) {
        List<Player> exceptList = new ArrayList<>(exceptPlayers);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (exceptList.contains(player)) continue;
            player.sendMessage(message);
        }
    }
    
}
