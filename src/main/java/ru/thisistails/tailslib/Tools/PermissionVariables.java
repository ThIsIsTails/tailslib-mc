package ru.thisistails.tailslib.Tools;

import java.util.List;
import java.util.Optional;

import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachmentInfo;

/**
 * Tool for Bukkit API permissions.
 */
public class PermissionVariables {
    
    /**
     * Returns permission variable.
     * <p>
     * Variables for Permission is a way to more flexibly customize permissions for your plugin.
     * 
     * To use Permission variables, you need to put a label on the line to indicate where your variable is located.
     * <p>
     * For example: {@code tailslib.permissionexample.%var%.other}
     * </p>
     * The {@code %var%} label indicates where the variable we are working with is located. Then we just need to call one of the methods and get our variable.
     * </p>
     * @param player        Player who have this permission
     * @param permission    Permission. Declare variable with {@code %var%}.
     * @return              Optional int, empty if permissions was not found
     * @throws NumberFormatException if the string cannot be parsed as an integer.
     */
    public static Optional<Integer> getIntPermissionVariable(Player player, String permission) throws NumberFormatException {
        List<String> givenPerm = List.of(permission.split("\\."));

        if (!givenPerm.contains("%var%")) throw new IllegalArgumentException("No %var% label in given permission: " + permission);

        for (PermissionAttachmentInfo perm : player.getEffectivePermissions()) {
            List<String> stringPerm = List.of(perm.getPermission().split("\\."));

            if (stringPerm.isEmpty()) continue;
            if (stringPerm.size() != givenPerm.size()) continue;

            for (int i = 0; i < stringPerm.size(); i++) {
                String p1 = stringPerm.get(i);
                String p2 = givenPerm.get(i);

                if (p2.equals("%var%"))
                    return Optional.of(Integer.valueOf(p1));
                if (!p1.equals(p2)) 
                    break;
            }
        }

        return Optional.empty();
    }

    /**
     * Returns permission variable.
     * <p>
     * Variables for Permission is a way to more flexibly customize permissions for your plugin.
     * 
     * To use Permission variables, you need to put a label on the line to indicate where your variable is located.
     * <p>
     * For example: {@code tailslib.permissionexample.%var%.other}
     * </p>
     * The {@code %var%} label indicates where the variable we are working with is located. Then we just need to call one of the methods and get our variable.
     * </p>
     * @param player        Player who have this permission
     * @param permission    Permission. Declare variable with {@code %var%}.
     * @return              Optional String, empty if permissions was not found
     */
    public static Optional<String> getStringPermissionVariable(Player player, String permission) {
        List<String> givenPerm = List.of(permission.split("\\."));

        if (!givenPerm.contains("%var%")) throw new IllegalArgumentException("No %var% label in given permission: " + permission);

        for (PermissionAttachmentInfo perm : player.getEffectivePermissions()) {
            List<String> stringPerm = List.of(perm.getPermission().split("\\."));

            if (stringPerm.isEmpty()) continue;
            if (stringPerm.size() != givenPerm.size()) continue;

            for (int i = 0; i < stringPerm.size(); i++) {
                String p1 = stringPerm.get(i);
                String p2 = givenPerm.get(i);

                if (p2.equals("%var%"))
                    return Optional.of(p1);

                if (!p1.equals(p2)) 
                    break;
            }
        }

        return Optional.empty();
    }

    /**
     * Returns permission variable.
     * <p>
     * Variables for Permission is a way to more flexibly customize permissions for your plugin.
     * 
     * To use Permission variables, you need to put a label on the line to indicate where your variable is located.
     * <p>
     * For example: {@code tailslib.permissionexample.%var%.other}
     * </p>
     * The {@code %var%} label indicates where the variable we are working with is located. Then we just need to call one of the methods and get our variable.
     * </p>
     * @param player        Player who have this permission
     * @param permission    Permission. Declare variable with {@code %var%}.
     * @return              Optional boolean, empty if permissions was not found
     */
    public static Optional<Boolean> getBooleanPermissionVariable(Player player, String permission) {
        List<String> givenPerm = List.of(permission.split("\\."));

        if (!givenPerm.contains("%var%")) throw new IllegalArgumentException("No %var% label in given permission: " + permission);

        for (PermissionAttachmentInfo perm : player.getEffectivePermissions()) {
            List<String> stringPerm = List.of(perm.getPermission().split("\\."));

            if (stringPerm.isEmpty()) continue;
            if (stringPerm.size() != givenPerm.size()) continue;

            for (int i = 0; i < stringPerm.size(); i++) {
                String p1 = stringPerm.get(i).toLowerCase();
                String p2 = givenPerm.get(i);

                if (p2.equals("%var%"))
                    if (p1.equals("yes") || p1.equals("true"))
                        return Optional.of(true);
                    else if (p1.equals("no") || p1.equals("false"))
                        return Optional.of(false);

                if (!p1.equals(p2)) 
                    break;
                    
            }
        }

        return Optional.empty();
    }

}
