package ru.thisistails.tailslib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.thisistails.tailslib.Tools.ChatTools;
import ru.thisistails.tailslib.Tools.YAMLManager;
import ru.thisistails.tailslib.Utils.Phrase;

/**
 * Class for tlib messages.
 */
public class Localization {
    
    public final static Phrase phrase;

    static {
        String langCode = YAMLManager.getAndTranslateString("TailsLib", "config.yml", "localization");

        if (langCode == null) {
            Logger log = LoggerFactory.getLogger("TailsLib");
            log.error("YOUR CONFIG IS BROKEN! DELETE EVERYTHING IN TailsLib FOLDER AND RESTART THE SERVER.");
            log.error("YOUR CONFIG IS BROKEN! DELETE EVERYTHING IN TailsLib FOLDER AND RESTART THE SERVER.");
            log.error("YOUR CONFIG IS BROKEN! DELETE EVERYTHING IN TailsLib FOLDER AND RESTART THE SERVER.");
            phrase = new Phrase("TailsLib", "Locales/locale-en_US.yml");
        } else {
            phrase = new Phrase("TailsLib", String.format("Locales/locale-%s.yml", langCode));
        }
    }

    public final static String prefix = phrase.getPhrase("prefix", ChatTools.translateString("[&cLOCALE FILE IS BROKEN OR NOT FOUND!!!&r]"));
    
    public final static String onEffectApply = phrase.getPhrase("effects.onApply", "Effect &e%effect_name%&r &8(%effect_level%)&r &bapplied&r to you for &e%effect_duration% seconds&f.");
    public final static String onEffectEnd = phrase.getPhrase("effects.onEffectEnd", "Effect &e%effect_name%&r is over.");
    public final static String onEffectInterruption = phrase.getPhrase("effects.onInterruption", "The &e%effect_name%&r effect has been &cinterrupted&e and has been removed from you.");
    public final static String noEffects = phrase.getPhrase("effects.placeholders.effectsNoEffects", "No effects.");
    public final static String effectAppliedSuccessfully = phrase.getPhrase("effects.commands.effectApplied", "&bYou&r &eapplied&r effect with ID &e%effect_id%&r to player named &e%player%&r.");
    public final static String allEffectsCleared = phrase.getPhrase("effects.commands.allEffectsCleared", "All effects has been cleared from %player%");
    public final static String effectCleared = phrase.getPhrase("effects.commands.effectCleared", "Effect %effect_id% cleared from %player%");

    public final static String itemNotFound = phrase.getPhrase("items.itemNotFound", "Item with ID &e%customitem_id%&r not found.");
    public final static String itemGiven = phrase.getPhrase("items.giveSuccess", "Given &e%customitem_id% to %player%");

    public final static String noEntryWhileLoadingMessage = phrase.getPhrase("errors.noEntryWhileLoadingMessage", "[TailsLib]\nYour server has broken config!");
    public final static String noPermissions = phrase.getPhrase("errors.noPermissions", "No permissions to execute this command.");
    public final static String somethingWentWrong = phrase.getPhrase("errors.somethingWentWrong", "&cSomethign went wrong while executing this action.");

    public final static String itemNotRegistered = phrase.getPhrase("errors.itemNotRegistered", "Item with ID &e%customitem_id%&r is not registered. &8[Player: %player%]");
    public final static String onlyPlayers = phrase.getPhrase("errors.onlyPlayerAllowed", "Only players allowed to use this command.");
    public final static String playerNotFound = phrase.getPhrase("errors.playerNotFound", "Player &e%player%&r not found.");
    public final static String effectNotFound = phrase.getPhrase("effects.commands.effectNotFound", "Effect with ID &e%effect_id%&r not found or not registered yet.");
    public final static String numberFormatException = phrase.getPhrase("errors.numberFormatException", "Can't parse numbers from arguments.");

}
