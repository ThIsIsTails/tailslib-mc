package ru.thisistails.tailslib;

import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import ru.thisistails.tailslib.Commands.EffectCommand;
import ru.thisistails.tailslib.Commands.GiveCItem;
import ru.thisistails.tailslib.Commands.ListItems;
import ru.thisistails.tailslib.Commands.PBlocksCommand;
import ru.thisistails.tailslib.Commands.PropertiesOnItem;
import ru.thisistails.tailslib.Commands.SettingsCommand;
import ru.thisistails.tailslib.CustomBlocks.CustomBlockListener;
import ru.thisistails.tailslib.CustomBlocks.CustomBlockManager;
import ru.thisistails.tailslib.CustomBlocks.Tests.TestBlock;
import ru.thisistails.tailslib.CustomEffects.CustomEffectListener;
import ru.thisistails.tailslib.CustomEffects.CustomEffectManager;
import ru.thisistails.tailslib.CustomEffects.Tests.SayHelloEffect;
import ru.thisistails.tailslib.CustomEffects.Tests.TickBasedEffect;
import ru.thisistails.tailslib.CustomItems.CustomItemListener;
import ru.thisistails.tailslib.CustomItems.CustomItemManager;
import ru.thisistails.tailslib.CustomItems.Tests.SimpleCustomBlockItem;
import ru.thisistails.tailslib.CustomItems.Tests.SimpleItem;
import ru.thisistails.tailslib.Listeners.StartLoadingAndSavingListener;
import ru.thisistails.tailslib.PlaceholderAPI.EffectDuration;
import ru.thisistails.tailslib.PlaceholderAPI.EffectIDGetter;
import ru.thisistails.tailslib.PlaceholderAPI.EffectsFormattedPlaceholder;
import ru.thisistails.tailslib.PlaceholderAPI.EffectsRawPlaceholder;
import ru.thisistails.tailslib.Tools.Config;
import ru.thisistails.tailslib.Tools.YAMLManager;
import ru.thisistails.tailslib.Utils.TLibListeners.TailsLibListeners;
import ru.thisistails.tailslib.Utils.TLibListeners.Tests.TestListener;

/**
 * Entry point for servers.
 */
public class TailsLib extends JavaPlugin {

	@Override
	public void onDisable() {
		getLogger().info("Saving custom blocks before disabling...");
		CustomBlockManager.getInstance().savePlacedBlocksDatas();
		getLogger().info("Saving custom effects before disabling...");
		CustomEffectManager.getInstance().saveEffects();
	}

	@Override
	public void onEnable() {
		new Metrics(this, 22145); // BStats
		Config.reloadConfig();
		YAMLManager.require(this.getName(), "Locales/locale-ru_RU.yml");
		YAMLManager.require(this.getName(), "Locales/locale-en_US.yml");

		getLogger().info("<----------------------------->");
		getLogger().info("I must warn you that TailsLib is in development right now.");
		getLogger().info("Newer versions can contain breaking changes to your code/config.");
		getLogger().info("Check change log to be sure that nothing will break.");
		getLogger().info("<----------------------------->");

		if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") == null) {
			getLogger().info("<--------------------------------->");
			getLogger().info("TailsLib supports PlaceholderAPI.");
			getLogger().info("You should install PlaceholderAPI for using placeholders in other plugins that not use TailsLib.");
			getLogger().info("<--------------------------------->");
		}
		else {
			new EffectDuration().register();
			new EffectIDGetter().register();
			new EffectsFormattedPlaceholder().register();
			new EffectsRawPlaceholder().register();
			getLogger().info("Placeholders registered.");
			getLogger().info("TailsLib still have poor PAPi support. Its in WIP!");
		}

		getServer().getPluginManager().registerEvents(new CustomItemListener(), this);
		getServer().getPluginManager().registerEvents(new CustomBlockListener(), this);
		getServer().getPluginManager().registerEvents(new StartLoadingAndSavingListener(), this);
		getServer().getPluginManager().registerEvents(CustomEffectListener.getListener(), this);
		
		getLogger().info("Trying to register test things.");
		if (Config.getConfig().getBoolean("tests.registerTests")) {
			CustomItemManager.register(new SimpleItem());
			CustomItemManager.register(new SimpleCustomBlockItem());
			
			CustomBlockManager.register(new TestBlock());

			CustomEffectManager.register(new SayHelloEffect());
			CustomEffectManager.register(new TickBasedEffect());
			TailsLibListeners.register(new TestListener());
			getLogger().info("Test things registered.");
		} else
			getLogger().info("tests.registerTests returned false. Skipping...");
		
		
		getCommand("listitems").setExecutor(new ListItems());
		getCommand("citem").setExecutor(new GiveCItem());
		getCommand("citem").setTabCompleter(new GiveCItem());
		
		getCommand("tailslib").setExecutor(new SettingsCommand());
		getCommand("tailslib").setTabCompleter(new SettingsCommand());
		
		getCommand("cblockplaced").setExecutor(new PBlocksCommand());
		getCommand("customeffect").setExecutor(new EffectCommand());
		getCommand("customeffect").setTabCompleter(new EffectCommand());

		getCommand("citemprops").setExecutor(new PropertiesOnItem());
		getLogger().info("Commands registered.");
	}
	
}
