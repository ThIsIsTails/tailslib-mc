package ru.thisistails.tailslib.CustomBlocks.Data;

import java.util.UUID;

import org.bukkit.Location;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import ru.thisistails.tailslib.CustomBlocks.CustomBlock;

/**
 * Basic data of placed block in world.
 */
@Data
public class PlacedBlockData {

    /**
     * UUID of placed block.
     */
    private @NotNull @Setter(AccessLevel.NONE) UUID uuid;

    /**
     * Location where block is placed.
     */
    private @Setter(AccessLevel.NONE) @NotNull Location location;
    // Может быть null если блок был поставлен командой.
    /**
     * UUID of player that placed this block.
     */
    private @Nullable UUID ownerUuid;
    /**
     * Custom block that represents this placed block.
     */
    private @Setter(AccessLevel.NONE) @NotNull CustomBlock placedBlock;
    
}
