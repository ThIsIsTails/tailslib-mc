package ru.thisistails.tailslib.CustomBlocks.Data;

import org.bukkit.Material;
import org.jetbrains.annotations.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import ru.thisistails.tailslib.CustomItems.CustomItem;

/**
 * Basic data of every custom block.
 */
@Data
public class CustomBlockData {

    /**
     * Item that represents this block.
     */
    private @Setter(AccessLevel.NONE) @NotNull CustomItem item;
    /**
     * ID of this block in TailsLib.
     */
    private @Setter(AccessLevel.NONE) @NotNull String blockId;
    /**
     * Material of this block. Material of item and block can be different but
     * item material will be replaced by block material when placed.
     */
    private @Setter(AccessLevel.NONE) @NotNull Material blockMaterial;

}
