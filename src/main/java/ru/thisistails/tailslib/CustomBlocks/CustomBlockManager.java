package ru.thisistails.tailslib.CustomBlocks;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import ru.thisistails.tailslib.CustomBlocks.Data.CustomBlockData;
import ru.thisistails.tailslib.CustomBlocks.Data.PlacedBlockData;
import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.Exceptions.IDPatternException;
import ru.thisistails.tailslib.Tools.Debug;

/**
 * Manager class for CustomBlocks
 */
public class CustomBlockManager {

    private static @Getter CustomBlockManager instance;
    private static final Logger logger = LoggerFactory.getLogger("CustomBlockManager");

    static {
        instance = new CustomBlockManager();
    }

    private Map<String, CustomBlock> blocks;
    private PlacedBlocks placedBlocks = new PlacedBlocks();
    private static Pattern idPattern = Pattern.compile("^[a-z_]+$");

    private CustomBlockManager() {
        blocks = new HashMap<>();
    }

    public static PlacedBlocks getPlacedBlocks() {
        return instance.placedBlocks;
    }

    /**
     * Returns a copy of all registered blocks.
     * @return  Map with all registered blocks.
     */
    public static Map<String, CustomBlock> getBlocks() {
        return new HashMap<>(instance.blocks);
    }

    /**
     * Registers your block.
     * @param block Your block.
     * @throws IDPatternException   If block id is not matching the pattern ({@code ^[a-z_]+$}).
     * @throws IllegalStateException    If property {@link Material#isBlock()} of {@link CustomBlockData#getItem()} or {@link CustomBlockData#getBlockMaterial()} is false.
     */
    public static void register(@NotNull CustomBlock block) {
        if (instance.blocks.containsValue(block)) {
            logger.warn("Seems like " + block.blockData().getBlockId() + " is registering again. Skipping...");
            return;
        }

        String id = block.blockData().getBlockId();

        Matcher matcher = idPattern.matcher(id);

        if (!matcher.find())
            throw new IDPatternException(id + " is not matching pattern " + idPattern.pattern());

        if (!block.blockData().getBlockMaterial().isBlock()) {
            throw new IllegalStateException("Material of block with ID " + block.blockData().getBlockId() + " is not a block!");
        }

        if (!block.blockData().getItem().itemData().getMaterial().isBlock()) {
            throw new IllegalStateException("Material of block item with ID " + block.blockData().getItem().itemData().getId() + " is not a block! Skipping.");
        }

        instance.blocks.put(block.blockData().getBlockId(), block);
        logger.info("Custom block " + block.blockData().getBlockId() + " registered.");
    }

    /**
     * Triggers block loading.
     * <p>
     * This function will not save blocks. You must save block before loading.
     * </p>
     * @apiNote Do not use this if you don't understand what are you doing.
     */
    public void loadBlocks() {
        placedBlocks = PlacedBlocks.load();
        
        if (placedBlocks == null) {
            logger.info("Failed to load placed blocks.");
            logger.info("Creating new PlacedBlocks instance. Try to use your backup.");
            placedBlocks = new PlacedBlocks();
        }

        logger.info("Starting block validation...");
        validateBlocks();
    }

    /**
     * Block validating function.
     */
    private synchronized void validateBlocks() {
        int position = 0;

        List<PlacedBlockData> cloneDatas;
        cloneDatas = new ArrayList<>(placedBlocks.getPlacedBlocks());

        for (PlacedBlockData data : cloneDatas) {
            CustomBlock block = data.getPlacedBlock();
            Location location = data.getLocation();
            Block inWorldBlock = location.getWorld().getBlockAt(location);

            if (inWorldBlock.getType() == Material.AIR) {
                logger.error(String.format("[Index: %d] Validation failed! Block at %f, %f, %f is AIR!", position, location.getX(), location.getY(), location.getZ()));
                validateFailedTryCatch(data, block);
                removePlacedBlock(data);
                position++;
                continue;
            }

            if (inWorldBlock.getType() != block.blockData().getBlockMaterial()) {
                logger.error(String.format("[Index: %d] Validation failed! Block at %f, %f, %f is not original material!", position, location.getX(), location.getY(), location.getZ()));
                validateFailedTryCatch(data, block);
                removePlacedBlock(data);
                position++;
                continue;
            }

            Debug.info("Block on position " + position + " is good");
            position++;
        }

        logger.info("Finished validating " + String.valueOf(position) + " blocks.");
    }

    private void validateFailedTryCatch(PlacedBlockData data, CustomBlock block) {
        try {
            block.validationFailed(data);
        } catch(Exception error) {
            logger.error("Got exception while calling ValidationFailed function for " + block.blockData().getBlockId(), error);
        }
    }

    /**
     * Save blocks with block validation.
     * <p>
     * Block saving will be executed after validating.
     * </p>
     */
    public synchronized void savePlacedBlocksDatas() {
        logger.info("Validating blocks before saving.");
        validateBlocks();
        
        saveWithoutValidation();
    }

    /**
     * Save block without validation.
     */
    public void saveWithoutValidation() {
        try {
            File file = new File(PlacedBlocks.getSaveFilePath());
            if (!file.exists()) {
                if (!file.createNewFile()) 
                    logger.warn("Failed to create save file: " + file.getPath());
            }
            placedBlocks.save();
        } catch (IOException e) {
            logger.error("An error occurred while saving placed blocks data: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Registers placed block on the server.
     * <p>
     * This function will fail with warning in log.
     * Function will fail if this data already exists.
     * </p>
     * @param data  Data to register.
     */
    public void registerPlacedBlockData(PlacedBlockData data) {
        if (placedBlocks.containsPlacedBlock(data)) {
            logger.warn(data.toString() + " already exists. Skipping...");
            return;
        }

        placedBlocks.addPlacedBlock(data);
    }

    /**
     * Creating new placed block in location with owner.
     * <p>
     * Also will set block type to {@link CustomBlockData#getBlockMaterial()}.
     * </p>
     * <p>
     * This function will fail with warning in log.
     * Function will fail if this data already exists.
     * </p>
     * @param block Block to be placed
     * @param location  Location where block is gonna be placed
     * @param owner Owner of this block.
     */
    public static void placeBlock(@NotNull CustomBlock block, @NotNull Location location, @Nullable UUID owner) {
        PlacedBlockData data = new PlacedBlockData(UUID.randomUUID(), location, block);
        if (instance.placedBlocks.containsPlacedBlock(data)) {
            logger.warn(data.toString() + " already exists. Skipping...");
            return;
        }

        data.setOwnerUuid(owner);
        instance.registerPlacedBlockData(data);
        location.getBlock().setType(block.blockData().getBlockMaterial());
    }

    /**
     * Removing placed block and sets block type to air at the location.
     * @param data  Data to be removed
     * @return      False if this data already removed/not exists, true if success.
     */
    public static boolean removePlacedBlock(PlacedBlockData data) {
        if (!instance.placedBlocks.containsPlacedBlock(data))
            return false; // Это чтобы не менять блоки просто так
        
        data.getLocation().getBlock().setType(Material.AIR);
        instance.placedBlocks.removePlacedBlock(data);
        return true;
    }

    /**
     * Returns {@link CustomBlock} by linked {@link CustomItem}
     * @param item  Item that linked with block.
     * @return      {@link CustomBlock}
     */
    public static @Nullable CustomBlock getCustomBlockByCustomItem(CustomItem item) {
        for (CustomBlock cblock : instance.blocks.values()) {
            if (cblock.blockData().getItem() == item)
                return cblock;
        }

        return null;
    }
    
}
