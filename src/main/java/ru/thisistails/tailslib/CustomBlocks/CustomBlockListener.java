package ru.thisistails.tailslib.CustomBlocks;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import lombok.extern.slf4j.Slf4j;
import ru.thisistails.tailslib.CustomBlocks.Data.PlacedBlockData;
import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.CustomItems.CustomItemUtils;
import ru.thisistails.tailslib.Utils.TLibListeners.LibListener;
import ru.thisistails.tailslib.Utils.TLibListeners.TailsLibListeners;

@Slf4j
public class CustomBlockListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockPlaced(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        CustomItem item = CustomItemUtils.tryGetCItemFromItemStack(event.getItemInHand());
        if (item == null) {
            return;
        }

        CustomBlock block = CustomBlockManager.getCustomBlockByCustomItem(item);

        if (block == null)
            return;
        
        Location location = event.getBlockPlaced().getLocation();
        PlacedBlockData data = new PlacedBlockData(player.getUniqueId(), location, block);

        for (LibListener listeners : TailsLibListeners.getListeners()) {
            try {
                listeners.onBlockCreation(data, event);
            } catch (Exception e) {
                log.error("Failed to execute event for listener.", e);
            }
        }

        try {
            block.blockCreation(data, event);
        } catch (Exception error) {
            log.error("Something went wrong while calling onCreation event. Block ID: " + block.blockData().getBlockId(), error);
            event.setCancelled(true); // To return items that has been consumed.
            return;
        }

        if (event.isCancelled()) return;

        CustomBlockManager.placeBlock(block, location, player.getUniqueId());
        CustomBlockManager.getInstance().saveWithoutValidation();
        log.info("New block placed at location " + event.getBlockPlaced().getLocation() + " with ID " + block.blockData().getBlockId()
            + " by owner UUID " + player.getUniqueId());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockDestroyed(BlockBreakEvent event) {
        Player player = event.getPlayer();
        PlacedBlockData placedBlockData = CustomBlockManager.getPlacedBlocks().searchPlacedBlockData(event.getBlock().getLocation());

        if (placedBlockData == null) return;

        CustomItem citem = placedBlockData.getPlacedBlock().blockData().getItem();

        if (event.isDropItems() && !event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            event.setDropItems(false);
            ItemStack item = CustomItemUtils.createItem(citem);
            placedBlockData.getLocation().getWorld().dropItemNaturally(placedBlockData.getLocation(), item);
        }

        for (LibListener listeners : TailsLibListeners.getListeners()) {
            try {
                listeners.onBlockDestroy(placedBlockData, event);
            } catch (Exception e) {
                log.error("Failed to execute event for listener.", e);
            }
        }
        
        try {
            placedBlockData.getPlacedBlock().blockDestroy(placedBlockData, event);
        } catch (Exception error) {
            log.error("Block with ID " + placedBlockData.getPlacedBlock().blockData().getBlockId() + " occured an exception while destruction.", error);
        }

        if (event.isCancelled()) return;

        CustomBlockManager.removePlacedBlock(placedBlockData);
        CustomBlockManager.getInstance().saveWithoutValidation();

        final String blockId = placedBlockData.getPlacedBlock().blockData().getBlockId();
        log.info("Block destroyed at location " + placedBlockData.getLocation() + " with ID " + 
            blockId + " by owner with UUID: " + placedBlockData.getOwnerUuid() + " (By player: " + player.getName() + ")");
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onRightClickOnBlock(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();

        if (action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR) {
            Block clickedBlock = event.getClickedBlock();
            if (clickedBlock == null) return;
            PlacedBlockData data = CustomBlockManager.getPlacedBlocks().searchPlacedBlockData(clickedBlock.getLocation());

            if (data == null) return;

            if (event.getHand().equals(EquipmentSlot.HAND)) {
                if (event.getItem() != null) {

                    for (LibListener listeners : TailsLibListeners.getListeners()) {
                        try {
                            listeners.onRightClickOnBlockWithItem(data, player, event.getItem());
                        } catch (Exception e) {
                            log.error("Failed to execute event for listener.", e);
                        }
                    }

                    try {
                        data.getPlacedBlock().rightClickOnBlockWithItem(data, player, event.getItem());
                    } catch (Exception e) {
                        log.error("Failed to execute event for custom block.", e);
                    }
                } else {

                    for (LibListener listeners : TailsLibListeners.getListeners()) {
                        try {
                            listeners.onRightClickOnBlock(data, player);
                        } catch (Exception e) {
                            log.error("Failed to execute event for listener.", e);
                        }
                    }

                    try {
                        data.getPlacedBlock().rightClickOnBlock(data, player);
                    } catch (Exception e) {
                        log.error("Failed to execute event for custom block.", e);
                    }
                }
            }
        }
    }
}
