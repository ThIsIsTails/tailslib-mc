package ru.thisistails.tailslib.CustomBlocks;

import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import ru.thisistails.tailslib.CustomBlocks.Data.CustomBlockData;
import ru.thisistails.tailslib.CustomBlocks.Data.PlacedBlockData;

/**
 * Basic interface for CustomBlock.
 */
public interface CustomBlock {

    /**
     * Contain basic data for this block to work.
     * @return  Basic block data.
     */
    public @NotNull CustomBlockData blockData();

    /**
     * Calls when player create new custom block.
     * <p>
     * Cancel this event to prevent placing a block and return item to owner.
     * You can also remove item from owner by removing it from iventory after canceling the event.
     * </p>
     * @param event     Block place event for more control
     * @param data  Data where this event is triggered.
     * @implNote    For block UUID use {@link PlacedBlockData#getUuid()}
     */
    public default void blockCreation(PlacedBlockData data, BlockPlaceEvent event) {}
    /**
     * Calls when player wants to destroy this block
     * <p>
     * Cancel this event to prevent removing a block.
     * </p>
     * @param event Block break event for more control
     * @param data  Data where this event is triggered.
     * @implNote    For block UUID use {@link PlacedBlockData#getUuid()}
     */
    public default void blockDestroy(PlacedBlockData data, BlockBreakEvent event) {}

    /**
     * When player clicks on block without item.
     * @param player    Author of this click
     * @param data  Data where this event is triggered.
     * @implNote    For block UUID use {@link PlacedBlockData#getUuid()}
     */
    public default void rightClickOnBlock(PlacedBlockData data, Player player) {}

    /**
     * When player clicks on block with item.
     * @param player    Author of this click
     * @param data  Data where this event is triggered.
     * @implNote    For block UUID use {@link PlacedBlockData#getUuid()}
     */
    public default void rightClickOnBlockWithItem(PlacedBlockData data, Player player, ItemStack item) {
        this.rightClickOnBlock(data, player);
    }

    /**
     * Calls when TailsLib is saving all blocks data.
     * @return  Data which needs to be saved.
     * @apiNote It will not call {@link #loadData(JsonObject)} on load if this function returns null
     * @param data  Which placed block is saving right now.
     * @implNote    For block UUID use {@link PlacedBlockData#getUuid()}
     */
    public default JsonElement saveData(PlacedBlockData data) { return null; }

    /**
     * Calls when TailsLib is loading all blocks data.
     * @param element   Json element with saved data.
     * @apiNote It will not fire if {@link #saveData()} returns null.
     */
    public default void loadData(@NotNull JsonObject element) {}

    /**
     * Called when TailsLib failed to validate this block on
     * sertain position and its got deleted.
     * <p>
     * This event is uncancellable. Event called before deleting data.
     * </p>
     * @param data  Data that will be deleted.
     */
    public default void validationFailed(PlacedBlockData data) {}

}
