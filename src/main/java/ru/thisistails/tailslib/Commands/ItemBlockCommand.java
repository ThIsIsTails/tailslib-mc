package ru.thisistails.tailslib.Commands;

import java.util.Map.Entry;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import net.md_5.bungee.api.ChatColor;
import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.CustomItems.CustomItemManager;

public class ItemBlockCommand implements CommandExecutor {

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command arg1, @NotNull String arg2, @NotNull String[] args) {

        if (args.length < 1)
            return false;

        String option = args[0];

        switch (option) {
            case "regList":
                StringBuffer text = new StringBuffer();
                StringBuffer loadedItems = new StringBuffer();
                text.append("Registered blocked items [");
                int total = 0;
                for (Entry<String, CustomItem> items : CustomItemManager.getManager().getItems().entrySet()) {
                    CustomItem item = items.getValue();
                    if (CustomItemManager.isItemBlocked(item)) {
                        loadedItems.append(ChatColor.RED + item.itemData().getId() + ChatColor.RESET + " ");
                        total++;
                    }
                }
                text.append(total).append("]: ");
                if (total == 0)
                    text.append("No blocked items loaded or found.");
                else
                    text.append(loadedItems);
                sender.sendMessage(text.toString());
                return true;
            
            case "block":
                if (args.length < 2)
                    return false;

                String id = args[1];

                CustomItemManager.blockItem(id);
                sender.sendMessage("Successfully blocked item with ID: " + id);
                return true;

            case "unblock":
                if (args.length < 2)
                    return false;

                // im to lazy
                String idd = args[1];

                CustomItemManager.unblockItem(idd);
                sender.sendMessage("Successfully unblocked item with ID: " + idd);
                return true;
        
            default:
                return false;
        }
    }
    
}
