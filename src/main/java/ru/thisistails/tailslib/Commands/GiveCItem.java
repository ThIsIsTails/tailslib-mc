package ru.thisistails.tailslib.Commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.thisistails.tailslib.Localization;
import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.CustomItems.CustomItemManager;
import ru.thisistails.tailslib.CustomItems.CustomItemUtils;

public class GiveCItem implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command arg1, @NotNull String arg2, @NotNull String[] args) {
        if (args.length == 0) {
            return false;
        }

        if (args.length == 1 && !(sender instanceof Player))
            return false;

        boolean giveToAnotherPlayer = args.length == 2;

        String itemID = args[0];
        Player target = null;
        
        if (giveToAnotherPlayer) {
            target = Bukkit.getPlayer(args[1]);
        }

        if (target == null) {
            if (giveToAnotherPlayer) {
                sender.sendMessage(Localization.prefix + " " + Localization.playerNotFound.replace("%player%", args[1]));
                return true;
            } else {
                if (!(sender instanceof Player)) {
                    sender.sendMessage("You are not a player! Use /citem <player> to give citem to player.");
                    return true;
                } else target = (Player) sender;
            }
        }

        CustomItem citem = CustomItemManager.getItemByID(itemID);

        if (citem == null) {
            sender.sendMessage(Localization.prefix + " " + Localization.itemNotFound.replace("%customitem_id%", itemID));
            return true;
        }

        target.getInventory().addItem(CustomItemUtils.createItem(citem));

        sender.sendMessage(Localization.prefix + " " + Localization.itemGiven.replace("%customitem_id%", itemID).replace("%player%", target.getName()));

        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command arg1, @NotNull String arg2, @NotNull String[] args) {
        List<String> toReturn = new ArrayList<>();
        toReturn.addAll(CustomItemManager.getManager().getItems().keySet());

        if (args.length == 1)
            return toReturn;
        else return null;
    }
    
}
