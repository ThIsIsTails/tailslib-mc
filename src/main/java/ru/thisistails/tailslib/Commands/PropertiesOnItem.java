package ru.thisistails.tailslib.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import ru.thisistails.tailslib.CustomItems.CustomItem;
import ru.thisistails.tailslib.CustomItems.CustomItemUtils;
import ru.thisistails.tailslib.CustomItems.CustomItemProperty;

public class PropertiesOnItem implements CommandExecutor {

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command arg1, @NotNull String arg2,@NotNull String[] args) {

        if (!(sender instanceof Player)) return false;

        Player player = (Player) sender;

        CustomItem item;

        if (player.getActiveItem() == null) return false;

        item = CustomItemUtils.tryGetCItemFromItemStack(player.getInventory().getItemInMainHand());

        if (item == null) {
            player.sendMessage("Its not a CustomItem!");
            return true;
        }

        StringBuffer buffer = new StringBuffer();

        for (CustomItemProperty property : item.itemData().getProperties()) {
            buffer.append("Property: " + property.name() + " (" + property.id() + "[" + property.version() + "]); ");
        }

        if (item.itemData().getProperties().isEmpty())
            buffer.append("No properties attached to this item");

        player.sendMessage(buffer.toString());

        return true;
    }
    
}
